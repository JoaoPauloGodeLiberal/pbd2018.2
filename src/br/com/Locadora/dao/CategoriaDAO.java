package br.com.Locadora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.Locadora.model.Categoria;

public class CategoriaDAO extends SupremoDAO<Categoria> {
	EntityManagerFactory factory;
	EntityManager manager;
	
	public CategoriaDAO(){
		factory = HibernateSingleton.getInstance(HibernateSingleton.HIBERNATE_MYSQL);
		manager = factory.createEntityManager();
		this.setClasse(Categoria.class);
	}
	
	public Categoria consultaId(int id){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Categoria categoria = manager.find(Categoria.class, id);
			manager.getTransaction().commit();
			return categoria;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}
	
	@Override
	public boolean insert(Object object) {
		// TODO Auto-generated method stub
		return super.insert(object);
	}
	
	@Override
	public boolean update(Object object) {
		// TODO Auto-generated method stub
		return super.update(object);
	}
	
	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return super.delete(id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Categoria> consultaDescricao(String descricao){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select c from Categoria c where c.descricao like :param");
			query.setParameter("param", "%"+descricao+"%");
			List<Categoria> categorias = query.getResultList(); 
			manager.getTransaction().commit();
			return categorias;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Categoria> ListALL(){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select c from Categoria c");
			List<Categoria> categorias = query.getResultList(); 
			manager.getTransaction().commit();
			return categorias;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
		
	}
	
}
