package br.com.Locadora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.swing.JOptionPane;

import br.com.Locadora.model.TiposLocacao;

public class TiposLocacaoDAO extends SupremoDAO<TiposLocacao> {
	EntityManagerFactory factory;
	EntityManager manager;
	
	public TiposLocacaoDAO(){
		factory = HibernateSingleton.getInstance(HibernateSingleton.HIBERNATE_MYSQL);
		manager = factory.createEntityManager();
		this.setClasse(TiposLocacao.class);
	}
	
	public TiposLocacao consultaId(int id){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			TiposLocacao tiposLocacao = manager.find(TiposLocacao.class, id);
			manager.getTransaction().commit();
			return tiposLocacao;
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erro ao Buscar Tipo", "Erro Busca", JOptionPane.ERROR_MESSAGE);
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}
	
	
	@Override
	public boolean update(Object object) {
		// TODO Auto-generated method stub
		return super.update(object);
	}
	
	public boolean update(List<TiposLocacao> tiposLocacao){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			manager.merge(tiposLocacao.get(0));
			manager.merge(tiposLocacao.get(1));
			manager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return false;
		} finally {
			manager.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<TiposLocacao> Consulta(){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select t from TiposLocacao t");
			List<TiposLocacao> tiposLocacaos = query.getResultList(); 
			manager.getTransaction().commit();
			return tiposLocacaos;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erro ao Buscar Tipos", "Erro Busca", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
	}	
	
	@SuppressWarnings("unchecked")
	public List<TiposLocacao> ConsultaUpdate(){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select t from TiposLocacao t");
			List<TiposLocacao> tiposLocacaos = query.getResultList(); 
			return tiposLocacaos;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erro ao Buscar Tipos", "Erro Busca", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
	}
	
	public boolean commit(){
		try {
			manager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	
}
