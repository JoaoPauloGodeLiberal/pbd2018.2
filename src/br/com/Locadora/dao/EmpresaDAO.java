package br.com.Locadora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.Locadora.model.Empresa;

public class EmpresaDAO extends SupremoDAO<Empresa> {
	EntityManagerFactory factory;
	EntityManager manager;

	public EmpresaDAO(){
		factory = HibernateSingleton.getInstance(HibernateSingleton.HIBERNATE_MYSQL);
		manager = factory.createEntityManager();
		this.setClasse(Empresa.class);
	}

	public Empresa consultaId(int id){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Empresa emp = manager.find(Empresa.class, id);
			manager.getTransaction().commit();
			return emp;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}

	@Override
	public boolean insert(Object object) {
		// TODO Auto-generated method stub
		return super.insert(object);
	}

	@Override
	public boolean update(Object object) {
		// TODO Auto-generated method stub
		return super.update(object);
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return super.delete(id);
	}

	@SuppressWarnings("unchecked")
	public List<Empresa> consultaNome(String Nome){
		manager.getTransaction().begin();
		Query query = manager.createQuery("select e from Empresa e where e.nome like :param");
		query.setParameter("param", "%"+Nome+"%");
		List<Empresa> empresas = query.getResultList(); 
		manager.getTransaction().commit();
		return empresas;
	}

	@SuppressWarnings("unchecked")
	public List<Empresa> consultaEmpresas(){
		manager.getTransaction().begin();
		Query query = manager.createQuery("select e from Empresa e");
		List<Empresa>empresas = query.getResultList(); 
		manager.getTransaction().commit();
		return empresas;
	}

	@SuppressWarnings("unchecked")
	public List<Empresa> listALL(){
		manager.getTransaction().begin();
		Query query = manager.createQuery("select e from Empresa e");
		List<Empresa>empresas = query.getResultList(); 
		manager.getTransaction().commit();
		return empresas;
	}
}
