package br.com.Locadora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.Locadora.model.Empresa;
import br.com.Locadora.model.Usuario;

public class UsuarioDAO extends SupremoDAO<Usuario>{
	EntityManagerFactory factory;
	EntityManager manager;

	public UsuarioDAO(){
		factory = HibernateSingleton.getInstance(HibernateSingleton.HIBERNATE_MYSQL);
		manager = factory.createEntityManager();
		this.setClasse(Usuario.class);
	}
	

	public Usuario consultaId(int id){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Usuario usuario = manager.find(Usuario.class, id);
			manager.getTransaction().commit();
			return usuario;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}

	}

	@Override
	public boolean insert(Object object) {
		// TODO Auto-generated method stub
		return super.insert(object);
	}

	@Override
	public boolean update(Object object) {
		// TODO Auto-generated method stub
		return super.update(object);
	}

	@Override
	public synchronized void merge(Object object) {
		// TODO Auto-generated method stub
		super.merge(object);
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return super.delete(id);
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> consultaNome(String Nome){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select u from Usuario u where u.nome like :param");
			query.setParameter("param", "%"+Nome+"%");
			List<Usuario> usuarios = query.getResultList();
			manager.getTransaction().commit();
			return usuarios;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> consultaLogin(String Login){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select u from Usuario u where u.login like :param");
			query.setParameter("param", "%"+Login+"%");
			List<Usuario> usuarios = query.getResultList();
			manager.getTransaction().commit();
			return usuarios;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			manager.close();
		}
	}
	
	public Usuario getUserByLogin(String Login){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select u from Usuario u where u.login = :param");
			query.setParameter("param", ""+Login+"");
			Usuario usuarios = (Usuario) query.getSingleResult();
			manager.getTransaction().commit();
			return usuarios;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> listarTodos(){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query consulta = manager.createQuery("select usuario from Usuario usuario");
			List<Usuario> usuarios = consulta.getResultList();
			manager.getTransaction().commit();
			return usuarios;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			manager.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Empresa> consultaEmpresas(){
		manager = factory.createEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createQuery("select e from Empresa e");
		List<Empresa>empresas = query.getResultList(); 
		manager.getTransaction().commit();
		return empresas;
	}

	public Empresa consultaEmpresa(int id){
		manager = factory.createEntityManager();
		manager.getTransaction().begin();
		Empresa empresa = manager.find(Empresa.class, id);
		manager.getTransaction().commit();
		return empresa;
	}
}
