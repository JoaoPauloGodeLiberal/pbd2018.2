package br.com.Locadora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.Locadora.model.Locacao;
import br.com.Locadora.model.TiposLocacao;

public class LocacaoDAO extends SupremoDAO<Locacao> {
	EntityManagerFactory factory;
	EntityManager manager;

	public LocacaoDAO(){
		factory = HibernateSingleton.getInstance(HibernateSingleton.HIBERNATE_MYSQL);
		manager = factory.createEntityManager();
		this.setClasse(Locacao.class);
	}

	public Locacao consultaId(int id){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Locacao locacao = manager.find(Locacao.class, id);
			manager.getTransaction().commit();
			return locacao;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}

	public Locacao consultaUpdate(int id){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Locacao locacao = manager.find(Locacao.class, id);
			return locacao;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}

	@Override
	public boolean insert(Object object) {
		// TODO Auto-generated method stub
		return super.insert(object);
	}

	@Override
	public boolean update(Object object) {
		// TODO Auto-generated method stub
		return super.update(object);
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return super.delete(id);
	}

	@SuppressWarnings("unchecked")
	public List<Locacao> listALL(){
		manager.getTransaction().begin();
		Query query = manager.createQuery("select l from Locacao l where cancelada = 0");
		List<Locacao> locacoes = query.getResultList(); 
		manager.getTransaction().commit();
		return locacoes;
	}

	@SuppressWarnings("unchecked")
	public List<Locacao> consultaByCliente(String Nome){
		manager.getTransaction().begin();
		Query query = manager.createQuery("select l from Locacao l where cancelada = 0 and cliente.ID = (select c.ID from Cliente c where nome like :param)");
		query.setParameter("param", "%"+Nome+"%");
		List<Locacao> locacoes = query.getResultList(); 
		manager.getTransaction().commit();
		return locacoes;
	}

	@SuppressWarnings("unchecked")
	public List<TiposLocacao> listTipoLocacao(){
		manager.getTransaction().begin();
		Query query = manager.createQuery("select t from TiposLocacao t");
		List<TiposLocacao> tiposLocacaos = query.getResultList(); 
		manager.getTransaction().commit();
		return tiposLocacaos;
	}

	public boolean commit(){
		try {
			manager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

}
