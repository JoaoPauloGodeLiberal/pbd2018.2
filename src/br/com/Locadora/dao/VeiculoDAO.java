package br.com.Locadora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.Locadora.model.Veiculo;

public class VeiculoDAO extends SupremoDAO<Veiculo> {
	EntityManagerFactory factory;
	EntityManager manager;
	
	public VeiculoDAO(){
		factory = HibernateSingleton.getInstance(HibernateSingleton.HIBERNATE_MYSQL);
		manager = factory.createEntityManager();
		this.setClasse(Veiculo.class);
	}
	
	public Veiculo consultaId(int codigo){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Veiculo veiculo = manager.find(Veiculo.class, codigo);
			manager.getTransaction().commit();
			return veiculo;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}
	
	public Veiculo consultaIdUpdate(int codigo){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Veiculo veiculo = manager.find(Veiculo.class, codigo);
			return veiculo;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}
	
	@Override
	public boolean insert(Object object) {
		// TODO Auto-generated method stub
		return super.insert(object);
	}
	
	@Override
	public boolean update(Object object) {
		// TODO Auto-generated method stub
		return super.update(object);
	}
	
	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return super.delete(id);
	}
	
	public Veiculo consultaPlaca(String placa){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select v from Veiculo v where v.placa = :param");
			query.setParameter("param", placa);
			Veiculo veiculo = (Veiculo) query.getSingleResult(); 
			manager.getTransaction().commit();
			return veiculo;
		} catch (NoResultException noResultException) {
			manager.getTransaction().rollback();
			noResultException.printStackTrace();
			return null;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Veiculo> ListAll(){
		
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Query query = manager.createQuery("select v from Veiculo v");
			List<Veiculo> veiculos = query.getResultList(); 
			manager.getTransaction().commit();
			return veiculos;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	public boolean commit(){
		try {
			manager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
}
