package br.com.Locadora.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public abstract class SupremoDAO<T> {
	
	EntityManagerFactory factory;
	EntityManager manager;
	private Class<T> classe;
	
	public SupremoDAO() {
		factory = HibernateSingleton.getInstance(HibernateSingleton.HIBERNATE_MYSQL);
		manager = factory.createEntityManager();
	}
	public void setClasse(Class<T> classe) {
		this.classe = classe;
	}
	
	public boolean insert(Object object){
		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			manager.persist(object);
			manager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return false;
		} finally{
			manager.close();
		}
		
	}
	
	public boolean update(Object object){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			manager.merge(object);
			manager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return false;
		} finally {
			manager.close();
		}
	}
	
	public synchronized void merge(Object object) {
		try {

			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			manager.merge(object);
			manager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			manager.getTransaction().rollback();
		}
	}
	
	public boolean delete(int id){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			System.out.println(id);
			T entity = (T) manager.find(classe, id);
			System.out.println(entity);
			manager.remove(entity);
			manager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			manager.close();
		}

	}
	
	
	
	
//	public List<Object> listarTodos(){
//
//		try {
//			manager = factory.createEntityManager();
//			manager.getTransaction().begin();
//			Query consulta = manager.createQuery("select usuario from Usuario usuario");
//			List<Object> objects = consulta.getResultList();
//			manager.getTransaction().commit();
//			return objects;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		} finally {
//			manager.close();
//		}
//	}
}
