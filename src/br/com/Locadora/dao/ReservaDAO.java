package br.com.Locadora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.Locadora.model.Reserva;

public class ReservaDAO extends SupremoDAO<Reserva> {
	EntityManagerFactory factory;
	EntityManager manager;

	public ReservaDAO(){
		factory = HibernateSingleton.getInstance(HibernateSingleton.HIBERNATE_MYSQL);
		manager = factory.createEntityManager();
		this.setClasse(Reserva.class);
	}

	public Reserva consultaId(int id){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Reserva reserva = manager.find(Reserva.class, id);
			manager.getTransaction().commit();
			return reserva;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}

	public Reserva consultaUpdate(int id){

		try {
			manager = factory.createEntityManager();
			manager.getTransaction().begin();
			Reserva reserva = manager.find(Reserva.class, id);
			return reserva;
		} catch (Exception e) {
			e.printStackTrace();
			manager.getTransaction().rollback();
			return null;
		} finally{
			manager.close();
		}
	}

	@Override
	public boolean insert(Object object) {
		// TODO Auto-generated method stub
		return super.insert(object);
	}

	@Override
	public boolean update(Object object) {
		// TODO Auto-generated method stub
		return super.update(object);
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return super.delete(id);
	}

	@SuppressWarnings("unchecked")
	public List<Reserva> consultaNome(String Descricao){
		manager.getTransaction().begin();
		Query query = manager.createQuery("select r from Reserva r where cancelada = 0 and efetivada = 0 and r.descricao like :param");
		query.setParameter("param", "%"+Descricao+"%");
		List<Reserva> reservas = query.getResultList(); 
		manager.getTransaction().commit();
		return reservas;
	}

	@SuppressWarnings("unchecked")
	public List<Reserva> listALL(){
		manager.getTransaction().begin();
		Query query = manager.createQuery("select r from Reserva r where cancelada = 0 and efetivada = 0");
		List<Reserva> reservas = query.getResultList(); 
		manager.getTransaction().commit();
		return reservas;
	}

	public boolean commit(){
		try {
			manager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
}
