package br.com.Locadora.view;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.Locadora.controller.ButtonHandlerConsultaLocacao;
import br.com.Locadora.dao.LocacaoDAO;


public class TelaConsultaLocacao extends JDialog {

	private static final long serialVersionUID = -7616001911230736284L;

	private JPanel contentPane;
	private JPanel panelTable;
	private JLabel labelCodigo;
	private JLabel labelDescricao;
	private JTextField fieldID;
	private JTextField fieldCliente;
	private JButton buttonResearch;
	private JButton buttonSelect;
	private JTable tableLocacao;
	private JSeparator separator;
	private JScrollPane scrollPaneTable;
	private DefaultTableModel modelTalble; 
	
	private LocacaoDAO locacaoDAO;
	
	private ButtonHandlerConsultaLocacao buttonHandlerConsultaLocacao;
	
	@SuppressWarnings("serial")
	public TelaConsultaLocacao(TelaLocacao telaLocacao) {
		setResizable(false);
		setTitle("Consulta Reservas");
		setType(Type.POPUP);
		setModal(true);
		setAlwaysOnTop(true);
		setSize(600, 315);
		setLocationRelativeTo(null);

		locacaoDAO = new LocacaoDAO();
		buttonHandlerConsultaLocacao = new ButtonHandlerConsultaLocacao(this, telaLocacao);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		labelCodigo = new JLabel("C�digo:");
		labelCodigo.setBounds(10, 15, 49, 14);
		contentPane.add(labelCodigo);

		fieldID = new JTextField(10);
		fieldID.setBounds(56, 12, 40, 20);
		contentPane.add(fieldID);
		fieldID.setColumns(10);

		labelDescricao = new JLabel("Cliente:");
		labelDescricao.setBounds(105, 14, 42, 16);
		contentPane.add(labelDescricao);

		fieldCliente = new JTextField();
		fieldCliente.setBounds(149, 12, 241, 20);
		contentPane.add(fieldCliente);
		fieldCliente.setColumns(10);

		buttonResearch = new JButton("Pesquisar");
		buttonResearch.addActionListener(buttonHandlerConsultaLocacao);
		buttonResearch.setBounds(392, 11, 92, 23);
		contentPane.add(buttonResearch);

		buttonSelect = new JButton("Selecionar");
		buttonSelect.setBounds(484, 11, 98, 23);
		buttonSelect.setEnabled(false);
		buttonSelect.addActionListener(buttonHandlerConsultaLocacao);
		contentPane.add(buttonSelect);

		separator = new JSeparator();
		separator.setBounds(10, 41, 572, 2);
		contentPane.add(separator);

		panelTable = new JPanel();
		panelTable.setBounds(10, 49, 572, 217);
		contentPane.add(panelTable);
		panelTable.setLayout(null);

		scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(0, 0, 572, 216);
		panelTable.add(scrollPaneTable);

		modelTalble = new DefaultTableModel(null,   
				new String [] {"C�digo", "Cliente", "Data", "Data Devolu��o"}){      

			boolean[] canEdit = new boolean []{false, false, false, false};        

			@Override  
			public boolean isCellEditable(int rowIndex, int columnIndex) {        
				return canEdit [columnIndex];        
			}      
		};    

		tableLocacao = new JTable(modelTalble);
		tableLocacao.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPaneTable.setViewportView(tableLocacao);
		tableLocacao.getColumnModel().getColumn(0).setPreferredWidth(70);
		tableLocacao.getColumnModel().getColumn(1).setPreferredWidth(499);
		tableLocacao.getColumnModel().getColumn(2).setPreferredWidth(70);
		tableLocacao.getColumnModel().getColumn(3).setPreferredWidth(70);

	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JPanel getPanelTable() {
		return panelTable;
	}

	public JLabel getLabelCodigo() {
		return labelCodigo;
	}

	public JLabel getLabelDescricao() {
		return labelDescricao;
	}

	public JTextField getFieldID() {
		return fieldID;
	}

	public JTextField getFieldCliente() {
		return fieldCliente;
	}

	public JButton getButtonResearch() {
		return buttonResearch;
	}

	public JButton getButtonSelect() {
		return buttonSelect;
	}

	public JTable getTableLocacao() {
		return tableLocacao;
	}

	public JSeparator getSeparator() {
		return separator;
	}

	public JScrollPane getScrollPaneTable() {
		return scrollPaneTable;
	}

	public DefaultTableModel getModelTalble() {
		return modelTalble;
	}

	public LocacaoDAO getLocacaoDAO() {
		return locacaoDAO;
	}

}
