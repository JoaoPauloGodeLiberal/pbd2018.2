package br.com.Locadora.view;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.Locadora.controller.ButtonHandlerConsultaCliente;


public class TelaConsultaCliente extends JDialog {

	private static final long serialVersionUID = -7616001911230736284L;

	private JPanel contentPane;
	private JPanel panelTable;
	private JLabel labelCodigo;
	private JLabel labelNome;
	private JTextField fieldID;
	private JTextField fieldNome;
	private JButton buttonResearch;
	private JButton buttonSelect;
	private JTable tableClientes;
	private JSeparator separator;
	private JScrollPane scrollPaneTable;
	private DefaultTableModel modelTalble; 

	//	private ClienteDAO clienteDAO;


	private ButtonHandlerConsultaCliente buttonHandlerConsultaCliente;

	@SuppressWarnings("serial")
	public TelaConsultaCliente(Object objectTela, boolean iscliente) {
		setResizable(false);
		setTitle("Consulta Clientes");
		setType(Type.POPUP);
		setModal(true);
		setAlwaysOnTop(true);
		setSize(600, 315);
		setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		labelCodigo = new JLabel("C\u00F3digo:");
		labelCodigo.setBounds(10, 15, 49, 14);
		contentPane.add(labelCodigo);

		fieldID = new JTextField(10);
		fieldID.setBounds(56, 12, 40, 20);
		contentPane.add(fieldID);
		fieldID.setColumns(10);

		labelNome = new JLabel("Nome:");
		labelNome.setBounds(105, 15, 40, 14);
		contentPane.add(labelNome);

		fieldNome = new JTextField();
		fieldNome.setBounds(145, 12, 245, 20);
		contentPane.add(fieldNome);
		fieldNome.setColumns(10);

		buttonResearch = new JButton("Pesquisar");

		buttonResearch.setBounds(392, 11, 92, 23);
		contentPane.add(buttonResearch);

		buttonSelect = new JButton("Selecionar");
		buttonSelect.setBounds(484, 11, 98, 23);
		buttonSelect.setEnabled(false);
		contentPane.add(buttonSelect);


		separator = new JSeparator();
		separator.setBounds(10, 41, 572, 2);
		contentPane.add(separator);

		panelTable = new JPanel();
		panelTable.setBounds(10, 49, 572, 217);
		contentPane.add(panelTable);
		panelTable.setLayout(null);

		scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(0, 0, 572, 216);
		panelTable.add(scrollPaneTable);

		modelTalble = new DefaultTableModel(null,   
				new String [] {"C�digo", "Nome", "Tipo Pessoa", "CPF/CNPJ", "Insc. Estadual", "Sexo"}){      

			boolean[] canEdit = new boolean []{false, false, false, false, false, false};        

			@Override  
			public boolean isCellEditable(int rowIndex, int columnIndex) {        
				return canEdit [columnIndex];        
			}      
		};    

		tableClientes = new JTable(modelTalble);
		tableClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPaneTable.setViewportView(tableClientes);
		tableClientes.getColumnModel().getColumn(0).setPreferredWidth(70);
		tableClientes.getColumnModel().getColumn(1).setPreferredWidth(350);
		tableClientes.getColumnModel().getColumn(2).setPreferredWidth(80);
		tableClientes.getColumnModel().getColumn(3).setPreferredWidth(100);
		tableClientes.getColumnModel().getColumn(4).setPreferredWidth(100);
		tableClientes.getColumnModel().getColumn(5).setPreferredWidth(75);


		buttonHandlerConsultaCliente = new ButtonHandlerConsultaCliente(this, objectTela, iscliente);

		buttonSelect.addActionListener(buttonHandlerConsultaCliente);
		buttonResearch.addActionListener(buttonHandlerConsultaCliente);

	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JPanel getPanelTable() {
		return panelTable;
	}

	public JLabel getLabelCodigo() {
		return labelCodigo;
	}

	public JLabel getLabelNome() {
		return labelNome;
	}

	public JTextField getFieldID() {
		return fieldID;
	}

	public JTextField getFieldNome() {
		return fieldNome;
	}

	public JButton getButtonResearch() {
		return buttonResearch;
	}

	public JButton getButtonSelect() {
		return buttonSelect;
	}

	public JTable getTableClientes() {
		return tableClientes;
	}

	public JSeparator getSeparator() {
		return separator;
	}

	public JScrollPane getScrollPaneTable() {
		return scrollPaneTable;
	}

	public DefaultTableModel getModelTalble() {
		return modelTalble;
	}

}
