package br.com.Locadora.view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import br.com.Locadora.model.FixedLengthJTextField;

public class TelaRecuperaSenha extends JFrame {

	/**
	 * @author joao_
	 */
	private static final long serialVersionUID = 1L;
	private JTextField emailf;
	private JTextField loginF;
	private JPasswordField passwordField;
	private JButton btnAtualizar;
	private JLabel lblRecuperarSenha;
	private JLabel lblEmail;
	private JLabel lblNome;
	private JLabel lblNovaSenha;

	public TelaRecuperaSenha() {
		super("Recuperar Senha");
		getContentPane().setLayout(null);
		getContentPane().setBackground(Color.BLACK);

		lblRecuperarSenha = new JLabel("Recuperar Senha");
		lblRecuperarSenha.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblRecuperarSenha.setBounds(43, 11, 201, 30);
		lblRecuperarSenha.setForeground(Color.BLUE);
		getContentPane().add(lblRecuperarSenha);

		lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEmail.setBounds(10, 68, 81, 26);
		lblEmail.setForeground(Color.BLUE);
		getContentPane().add(lblEmail);

		lblNome = new JLabel("Login:");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNome.setBounds(10, 105, 81, 30);
		lblNome.setForeground(Color.BLUE);
		getContentPane().add(lblNome);

		lblNovaSenha = new JLabel("Nova Senha:");
		lblNovaSenha.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNovaSenha.setBounds(10, 146, 117, 27);
		lblNovaSenha.setForeground(Color.BLUE);
		getContentPane().add(lblNovaSenha);

		emailf = new JTextField();
		emailf.setBounds(78, 65, 196, 30);
		emailf.setBackground(Color.BLACK);
		emailf.setForeground(Color.BLUE);
		emailf.setFont(new Font("Tahoma", Font.PLAIN, 20));
		getContentPane().add(emailf);
		emailf.setColumns(10);

		loginF = new JTextField();
		loginF.setBounds(78, 104, 196, 30);
		loginF.setBackground(Color.BLACK);
		loginF.setForeground(Color.BLUE);
		loginF.setFont(new Font("Tahoma", Font.PLAIN, 20));
		getContentPane().add(loginF);
		loginF.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(148, 143, 126, 30);
		passwordField.setDocument(new FixedLengthJTextField(6));
		passwordField.setBackground(Color.BLACK);
		passwordField.setForeground(Color.BLUE);
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		getContentPane().add(passwordField);

		btnAtualizar = new JButton("Atualizar");
		btnAtualizar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnAtualizar.setBackground(Color.BLACK);
		btnAtualizar.setForeground(Color.BLUE);
		btnAtualizar.setBounds(166, 211, 108, 39);
		getContentPane().add(btnAtualizar);

		setSize(300, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);

	}

	public JTextField getEmailf() {
		return emailf;
	}

	public JTextField getLoginF() {
		return loginF;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public JButton getBtnAtualizar() {
		return btnAtualizar;
	}


}
