package br.com.Locadora.view;


import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import br.com.Locadora.dao.CategoriaDAO;
import br.com.Locadora.dao.EmpresaDAO;
import br.com.Locadora.dao.ReservaDAO;
import br.com.Locadora.model.Categoria;
import br.com.Locadora.model.Cliente;
import br.com.Locadora.model.Empresa;
import br.com.Locadora.model.Reserva;


public class TelaReserva extends JInternalFrame {


	private static final long serialVersionUID = 3691713444891118750L;

	private JPanel contentPane;
	private JPanel panelCentro;
	private JLabel labelCodigo;
	private JLabel labelData;
	private JLabel labelStatus;
	private JLabel labelEmpresa;
	private JLabel labelCategoria;
	private JLabel labelCliente;
	private JLabel labelDescricao;
	private JLabel labelDataDeRetirada;
	private JTextField fieldCodigo;
	private JTextField fieldDescricao;
	private JTextField fieldData;
	private JTextField fieldStatus;
	private JTextField fieldCliente;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxEmpresa;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxCategoria;
	private JButton buttonSalvar;
	private JButton buttonCancelar;
	private JButton buttonExcluir;
	private JButton buttonLocalizar;
	private JButton buttonNovo;
	private JButton buttonLocalizarCliente;
	private JDateChooser dateChooserDataRetirada;
	
	private boolean saveupdate;

	private Date dataAtual;
	
	private EmpresaDAO empresaController;
	private CategoriaDAO categoriaDAO;
	private ReservaDAO reservaDAO;
	
	private Reserva reservaUpdate;
	private List<Categoria> categorias;
	private List<Empresa> empresas;
	private Cliente cliente;	
	private JCheckBox chckbxCancelarReserva;

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TelaReserva() {
		setTitle("Reservas                                                                                                                                                                            ");
		setClosable(true);
		setResizable(false);
		setBounds(100, 100, 635, 255);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		empresaController = new EmpresaDAO();
		categoriaDAO = new CategoriaDAO();
		reservaDAO = new ReservaDAO();

		panelCentro = new JPanel();
		panelCentro.setBounds(10, 11, 603, 165);
		contentPane.add(panelCentro);
		panelCentro.setLayout(null);

		labelData = new JLabel("Data:");
		labelData.setBounds(164, 15, 29, 16);
		panelCentro.add(labelData);

		labelStatus = new JLabel("Status:");
		labelStatus.setBounds(281, 15, 45, 16);
		panelCentro.add(labelStatus);

		labelEmpresa = new JLabel("Empresa:");
		labelEmpresa.setBounds(388, 92, 54, 16);
		panelCentro.add(labelEmpresa);

		comboBoxEmpresa = new JComboBox();
		empresas = empresaController.listALL();
		for(int i=0; i <empresas.size(); i++) {
			comboBoxEmpresa.addItem(empresas.get(i).getId()+" - "+empresas.get(i).getNome());
		}
		comboBoxEmpresa.setEnabled(false);
		comboBoxEmpresa.setBounds(444, 90, 147, 20);
		panelCentro.add(comboBoxEmpresa);

		labelCategoria = new JLabel("Categoria:");
		labelCategoria.setBounds(21, 92, 58, 16);
		panelCentro.add(labelCategoria);

		labelDescricao = new JLabel("Descri\u00E7\u00E3o:");
		labelDescricao.setBounds(15, 68, 64, 16);
		panelCentro.add(labelDescricao);

		fieldDescricao = new JTextField();
		fieldDescricao.setEditable(false);
		fieldDescricao.setBounds(82, 66, 306, 20);
		panelCentro.add(fieldDescricao);
		fieldDescricao.setColumns(10);

		fieldData = new JTextField();
		fieldData.setEditable(false);
		fieldData.setBounds(197, 13, 72, 20);
		panelCentro.add(fieldData);
		fieldData.setColumns(10);

		labelCodigo = new JLabel("C\u00F3digo:");
		labelCodigo.setBounds(37, 15, 42, 16);
		panelCentro.add(labelCodigo);

		fieldCodigo = new JTextField();
		fieldCodigo.setEditable(false);
		fieldCodigo.setBounds(82, 13, 64, 20);
		panelCentro.add(fieldCodigo);
		fieldCodigo.setColumns(10);
		
		fieldStatus = new JTextField();
		fieldStatus.setEditable(false);
		fieldStatus.setBounds(325, 13, 130, 20);
		panelCentro.add(fieldStatus);
		fieldStatus.setColumns(10);
		
		labelCliente = new JLabel("Cliente:");
		labelCliente.setBounds(34, 43, 45, 16);
		panelCentro.add(labelCliente);
		
		fieldCliente = new JTextField();
		fieldCliente.setEditable(false);
		fieldCliente.setBounds(82, 41, 288, 20);
		panelCentro.add(fieldCliente);
		fieldCliente.setColumns(10);
		
		buttonLocalizarCliente = new JButton("Pesquisar");
		buttonLocalizarCliente.setEnabled(false);
		buttonLocalizarCliente.setToolTipText("Pesquisar");
		buttonLocalizarCliente.setBounds(382, 38, 86, 26);
		panelCentro.add(buttonLocalizarCliente);
		
		comboBoxCategoria = new JComboBox();
		comboBoxCategoria.setEnabled(false);
		comboBoxCategoria.setBounds(82, 90, 147, 20);
		categoriaDAO = new CategoriaDAO();
		categorias = categoriaDAO.ListALL();
		for (int i = 0; i < categorias.size(); i++) {
			comboBoxCategoria.addItem(categorias.get(i).getDescricao());
		}
		panelCentro.add(comboBoxCategoria);
		
		labelDataDeRetirada = new JLabel("Data de Retirada:");
		labelDataDeRetirada.setBounds(400, 68, 97, 16);
		panelCentro.add(labelDataDeRetirada);
		
		dateChooserDataRetirada = new JDateChooser();
		dataAtual = dateChooserDataRetirada.getDate();
		dateChooserDataRetirada.setEnabled(false);
		dateChooserDataRetirada.setBounds(499, 66, 92, 20);
		panelCentro.add(dateChooserDataRetirada);
		
		chckbxCancelarReserva = new JCheckBox("Cancelar Reserva");
		chckbxCancelarReserva.setEnabled(false);
		chckbxCancelarReserva.setBounds(465, 11, 126, 24);
		panelCentro.add(chckbxCancelarReserva);

		buttonNovo = new JButton("Novo");
		buttonNovo.setToolTipText("Novo");
		buttonNovo.setBounds(160, 187, 86, 26);
		contentPane.add(buttonNovo);
		
		buttonSalvar = new JButton("Salvar");
		buttonSalvar.setToolTipText("Salvar");
		buttonSalvar.setEnabled(false);
		buttonSalvar.setBounds(256, 187, 86, 26);
		contentPane.add(buttonSalvar);
		
		buttonExcluir = new JButton("Excluir");
		buttonExcluir.setToolTipText("Excluir");
		buttonExcluir.setEnabled(false);
		buttonExcluir.setBackground(SystemColor.inactiveCaption);
		buttonExcluir.setBounds(352, 187, 80, 26);
		contentPane.add(buttonExcluir);
		
		buttonCancelar = new JButton("Desfazer");
		buttonCancelar.setToolTipText("Desfazer");
		buttonCancelar.setBounds(442, 187, 75, 26);
		contentPane.add(buttonCancelar);
		
		buttonLocalizar = new JButton("Pesquisar");
		buttonLocalizar.setToolTipText("Localizar");
		buttonLocalizar.setBounds(527, 187, 86, 26);
		contentPane.add(buttonLocalizar);
		
		buttonNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cleanFields();
				buttonSalvar.setEnabled(true);
				buttonExcluir.setEnabled(false);
				buttonLocalizar.setEnabled(false);
				buttonNovo.setEnabled(false);
				enableFields();
				saveupdate = true;
			}
		});
		
		buttonSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (validarFields()) {
					
					if(saveupdate){ 
						Reserva reserva = new Reserva();
						reserva.setCliente(cliente);
						reserva.setCategoria(categorias.get(comboBoxCategoria.getSelectedIndex()));
						reserva.setEmpresa(empresas.get(comboBoxEmpresa.getSelectedIndex()));
						reserva.setDescricao(fieldDescricao.getText());
						reserva.setDataRetirada(dateChooserDataRetirada.getDate());
						if(reservaDAO.insert(reserva)){
							JOptionPane.showMessageDialog(null, "Reserva Realizada com Sucesso", "Mensagem Reserva", JOptionPane.INFORMATION_MESSAGE);
							cleanFields();
							disableFields();
							buttonNovo.setEnabled(true);
							buttonExcluir.setEnabled(false);
							buttonSalvar.setEnabled(false);
							buttonLocalizar.setEnabled(true);
						}
						else
							JOptionPane.showMessageDialog(null, "Erro ao Realizar Reserva", "Erro Inser��o", JOptionPane.ERROR_MESSAGE);
					} else{ 
						reservaUpdate.setID(Integer.parseInt(fieldCodigo.getText()));
						reservaUpdate.setCliente(cliente);
						reservaUpdate.setCategoria(categorias.get(comboBoxCategoria.getSelectedIndex()));
						reservaUpdate.setEmpresa(empresas.get(comboBoxEmpresa.getSelectedIndex()));
						reservaUpdate.setDescricao(fieldDescricao.getText());
						reservaUpdate.setDataRetirada(dateChooserDataRetirada.getDate());
						reservaUpdate.setCancelada(chckbxCancelarReserva.isSelected());
						if(reservaDAO.commit()){
							JOptionPane.showMessageDialog(null, "Reserva Alterada com Sucesso", "Mensagem Reserva", JOptionPane.INFORMATION_MESSAGE);
							reservaUpdate = null;
							cleanFields();
							disableFields();
							buttonNovo.setEnabled(true);
							buttonExcluir.setEnabled(false);
							buttonSalvar.setEnabled(false);
							buttonLocalizar.setEnabled(true);
						}
						else
							JOptionPane.showMessageDialog(null, "Erro ao Alterar Reserva", "Erro Autaliza��o", JOptionPane.ERROR_MESSAGE);
						
					}
				}
			}	
		});
		
		buttonExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(null, "Deseja Realmente Excluir ?", "Excluir Reserva", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==0) {
					if(reservaDAO.delete(Integer.parseInt(fieldCodigo.getText()))){
						JOptionPane.showMessageDialog(null, "Reserva Exclu�da com Sucesso", "Mensagem Reserva", JOptionPane.INFORMATION_MESSAGE);
						cleanFields();
						disableFields();
						buttonNovo.setEnabled(true);
						buttonExcluir.setEnabled(false);
						buttonSalvar.setEnabled(false);
					}
					else
						JOptionPane.showMessageDialog(null, "Erro ao Deletar Reserva", "Erro Remo��o", JOptionPane.ERROR_MESSAGE);
					
				}
			}
		});

		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cleanFields();
				disableFields();
				buttonSalvar.setEnabled(false);
				buttonNovo.setEnabled(true);
				buttonExcluir.setEnabled(false);
				buttonLocalizar.setEnabled(true);
			}
		});

		buttonLocalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new TelaConsultaReserva(TelaReserva.this).setVisible(true);
			}
		});
		
		buttonLocalizarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new TelaConsultaCliente(TelaReserva.this, false).setVisible(true);
			}
		});
		
		setVisible(true);
	}

	private boolean validarFields(){
		if (fieldDescricao.getText().isEmpty()||fieldCliente.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Campos Obrigat�rios", null, JOptionPane.WARNING_MESSAGE);
			return false;
		}

		return true;
	}
	
	public void cleanFields(){
		fieldCodigo.setText(null);
		fieldDescricao.setText(null);
		fieldData.setText(null);
		fieldStatus.setText(null);
		fieldCliente.setText(null);
		comboBoxEmpresa.setSelectedIndex(0);
		comboBoxCategoria.setSelectedIndex(0);
		dateChooserDataRetirada.setDate(dataAtual);
		chckbxCancelarReserva.setSelected(false);
	}
	
	public void enableFields(){
		fieldDescricao.setEditable(true);
		comboBoxEmpresa.setEnabled(true);
		comboBoxCategoria.setEnabled(true);
		buttonLocalizarCliente.setEnabled(true);
		dateChooserDataRetirada.setEnabled(true);
		chckbxCancelarReserva.setEnabled(true);
	}
	
	public void disableFields(){
		fieldDescricao.setEditable(false);
		comboBoxEmpresa.setEnabled(false);
		comboBoxCategoria.setEnabled(false);
		buttonLocalizarCliente.setEnabled(false);
		dateChooserDataRetirada.setEnabled(false);
		chckbxCancelarReserva.setEnabled(false);
	}
	
	public void setFields(int id){
		reservaUpdate = reservaDAO.consultaUpdate(id);
		fieldCodigo.setText(String.valueOf(reservaUpdate.getID()));
		fieldDescricao.setText(reservaUpdate.getDescricao());
		fieldData.setText(String.valueOf(reservaUpdate.getData()));
		fieldStatus.setText(reservaUpdate.getStatus());
		fieldCliente.setText(reservaUpdate.getCliente().getNome());
		cliente = reservaUpdate.getCliente();
		comboBoxEmpresa.setSelectedItem(reservaUpdate.getEmpresa().getId()+" - "+reservaUpdate.getEmpresa().getNome());
		comboBoxCategoria.setSelectedItem(reservaUpdate.getCategoria().getDescricao());
		dateChooserDataRetirada.setDate(reservaUpdate.getDataRetirada());
		buttonExcluir.setEnabled(true);
		buttonSalvar.setEnabled(true);
		buttonNovo.setEnabled(false);
		saveupdate = false;
		enableFields();
		chckbxCancelarReserva.setEnabled(true);
	}
	
	public void setCliente(Cliente cliente){
		this.cliente = cliente;
		fieldCliente.setText(cliente.getId()+" - "+cliente.getNome());
	}
	
	public void setPosicao() {
		Dimension d = this.getDesktopPane().getSize();
		this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2); 
	}
}
