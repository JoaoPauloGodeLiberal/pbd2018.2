package br.com.Locadora.view;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.Locadora.controller.ButtonHandlerConsultaUsuario;


public class TelaConsultaUsuario extends JDialog {

	private static final long serialVersionUID = -7616001911230736284L;

	private JPanel contentPane;
	private JPanel panelTable;
	private JLabel labelCodigo;
	private JLabel labelNome;
	private JTextField fieldID;
	private JTextField fieldNome;
	private JButton buttonResearch;
	private JButton buttonSelect;
	private JTable tableUsuarios;
	private JSeparator separator;
	private JScrollPane scrollPaneTable;
	private DefaultTableModel modelTalble; 
	
	private ButtonHandlerConsultaUsuario buttonHandlerConsultaUsuario;

	@SuppressWarnings("serial")
	public TelaConsultaUsuario(TelaCadastroUsuarioOK telaCadastroUsuario) {
		
		setResizable(false);
		setTitle("Consulta Usu�rios");
		setType(Type.POPUP);
		setModal(true);
		setAlwaysOnTop(true);
		setSize(600, 315);
		setLocationRelativeTo(null);

		buttonHandlerConsultaUsuario = new ButtonHandlerConsultaUsuario(this, telaCadastroUsuario);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		labelCodigo = new JLabel("C�digo:");
		labelCodigo.setBounds(10, 15, 49, 14);
		contentPane.add(labelCodigo);

		fieldID = new JTextField(10);
		fieldID.setBounds(56, 12, 40, 20);
		contentPane.add(fieldID);
		fieldID.setColumns(10);

		labelNome = new JLabel("Nome:");
		labelNome.setBounds(105, 15, 40, 14);
		contentPane.add(labelNome);

		fieldNome = new JTextField();
		fieldNome.setBounds(145, 12, 245, 20);
		contentPane.add(fieldNome);
		fieldNome.setColumns(10);

		buttonResearch = new JButton("Pesquisar");
		buttonResearch.addActionListener(buttonHandlerConsultaUsuario);
		buttonResearch.setBounds(392, 11, 92, 23);
		contentPane.add(buttonResearch);

		buttonSelect = new JButton("Selecionar");
		buttonSelect.setEnabled(false);
		buttonSelect.addActionListener(buttonHandlerConsultaUsuario);
		buttonSelect.setBounds(484, 11, 98, 23);
		contentPane.add(buttonSelect);

		separator = new JSeparator();
		separator.setBounds(10, 41, 572, 2);
		contentPane.add(separator);

		panelTable = new JPanel();
		panelTable.setBounds(10, 49, 572, 217);
		contentPane.add(panelTable);
		panelTable.setLayout(null);

		scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(0, 0, 572, 216);
		panelTable.add(scrollPaneTable);

		modelTalble = new DefaultTableModel(null,   
				new String [] {"C�digo", "Login", "Nome"}){      

			boolean[] canEdit = new boolean []{false, false, false};        

			@Override  
			public boolean isCellEditable(int rowIndex, int columnIndex) {        
				return canEdit [columnIndex];        
			}      
		};    

		tableUsuarios = new JTable(modelTalble);
		tableUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPaneTable.setViewportView(tableUsuarios);
		tableUsuarios.getColumnModel().getColumn(0).setPreferredWidth(70);
		tableUsuarios.getColumnModel().getColumn(1).setPreferredWidth(170);
		tableUsuarios.getColumnModel().getColumn(2).setPreferredWidth(329);

	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JPanel getPanelTable() {
		return panelTable;
	}

	public JLabel getLabelCodigo() {
		return labelCodigo;
	}

	public JLabel getLabelNome() {
		return labelNome;
	}

	public JTextField getFieldID() {
		return fieldID;
	}

	public JTextField getFieldNome() {
		return fieldNome;
	}

	public JButton getButtonResearch() {
		return buttonResearch;
	}

	public JButton getButtonSelect() {
		return buttonSelect;
	}

	public JTable getTableUsuarios() {
		return tableUsuarios;
	}

	public JSeparator getSeparator() {
		return separator;
	}

	public JScrollPane getScrollPaneTable() {
		return scrollPaneTable;
	}

	public DefaultTableModel getModelTalble() {
		return modelTalble;
	}

	public ButtonHandlerConsultaUsuario getButtonHandlerConsultaUsuario() {
		return buttonHandlerConsultaUsuario;
	}
}
