package br.com.Locadora.view;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.Locadora.controller.ButtonHandlerConsultaReserva;
import br.com.Locadora.dao.ReservaDAO;


public class TelaConsultaReserva extends JDialog {

	private static final long serialVersionUID = -7616001911230736284L;

	private JPanel contentPane;
	private JPanel panelTable;
	private JLabel labelCodigo;
	private JLabel labelDescricao;
	private JTextField fieldID;
	private JTextField fieldDescricao;
	private JButton buttonResearch;
	private JButton buttonSelect;
	private JTable tableReservas;
	private JSeparator separator;
	private JScrollPane scrollPaneTable;
	private DefaultTableModel modelTalble; 
	
	private TelaReserva telaReserva;
	private TelaLocacao telaLocacao;
	private ReservaDAO reservaDAO;
	
	private  ButtonHandlerConsultaReserva buttonHandlerConsultaReserva;
	
	@SuppressWarnings("serial")
	public TelaConsultaReserva(Object objectTela) {
		setResizable(false);
		setTitle("Consulta Reservas");
		setType(Type.POPUP);
		setModal(true);
		setAlwaysOnTop(true);
		setSize(600, 315);
		setLocationRelativeTo(null);

		reservaDAO = new ReservaDAO();
		buttonHandlerConsultaReserva = new ButtonHandlerConsultaReserva(this, telaReserva, telaLocacao, objectTela);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		labelCodigo = new JLabel("C\u00F3digo:");
		labelCodigo.setBounds(10, 15, 49, 14);
		contentPane.add(labelCodigo);

		fieldID = new JTextField(10);
		fieldID.setBounds(56, 12, 40, 20);
		contentPane.add(fieldID);
		fieldID.setColumns(10);

		labelDescricao = new JLabel("Descri\u00E7\u00E3o:");
		labelDescricao.setBounds(105, 14, 61, 16);
		contentPane.add(labelDescricao);

		fieldDescricao = new JTextField();
		fieldDescricao.setBounds(169, 12, 221, 20);
		contentPane.add(fieldDescricao);
		fieldDescricao.setColumns(10);

		buttonResearch = new JButton("Pesquisar");
		buttonResearch.addActionListener(buttonHandlerConsultaReserva);
		buttonResearch.setBounds(392, 11, 92, 23);
		contentPane.add(buttonResearch);

		buttonSelect = new JButton("Selecionar");
		buttonSelect.setBounds(484, 11, 98, 23);
		buttonSelect.setEnabled(false);
		buttonSelect.addActionListener(buttonHandlerConsultaReserva);
		contentPane.add(buttonSelect);

		separator = new JSeparator();
		separator.setBounds(10, 41, 572, 2);
		contentPane.add(separator);

		panelTable = new JPanel();
		panelTable.setBounds(10, 49, 572, 217);
		contentPane.add(panelTable);
		panelTable.setLayout(null);

		scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(0, 0, 572, 216);
		panelTable.add(scrollPaneTable);

		modelTalble = new DefaultTableModel(null,   
				new String [] {"C�digo", "Descri��o", "Data", "Status"}){      

			boolean[] canEdit = new boolean []{false, false, false, false};        

			@Override  
			public boolean isCellEditable(int rowIndex, int columnIndex) {        
				return canEdit [columnIndex];        
			}      
		};    

		tableReservas = new JTable(modelTalble);
		tableReservas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPaneTable.setViewportView(tableReservas);
		tableReservas.getColumnModel().getColumn(0).setPreferredWidth(70);
		tableReservas.getColumnModel().getColumn(1).setPreferredWidth(499);
		tableReservas.getColumnModel().getColumn(2).setPreferredWidth(70);
		tableReservas.getColumnModel().getColumn(3).setPreferredWidth(70);

	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JPanel getPanelTable() {
		return panelTable;
	}

	public JLabel getLabelCodigo() {
		return labelCodigo;
	}

	public JLabel getLabelDescricao() {
		return labelDescricao;
	}

	public JTextField getFieldID() {
		return fieldID;
	}

	public JTextField getFieldDescricao() {
		return fieldDescricao;
	}

	public JButton getButtonResearch() {
		return buttonResearch;
	}

	public JButton getButtonSelect() {
		return buttonSelect;
	}

	public JTable getTableReservas() {
		return tableReservas;
	}

	public JSeparator getSeparator() {
		return separator;
	}

	public JScrollPane getScrollPaneTable() {
		return scrollPaneTable;
	}

	public DefaultTableModel getModelTalble() {
		return modelTalble;
	}

	public TelaReserva getTelaReserva() {
		return telaReserva;
	}

	public TelaLocacao getTelaLocacao() {
		return telaLocacao;
	}

	public ReservaDAO getReservaDAO() {
		return reservaDAO;
	}

	public ButtonHandlerConsultaReserva getButtonHandlerConsultaReserva() {
		return buttonHandlerConsultaReserva;
	}
}
