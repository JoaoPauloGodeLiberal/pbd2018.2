package br.com.Locadora.view;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

import br.com.Locadora.controller.ButtonHandlerConsultaVeiculos;
import br.com.Locadora.dao.VeiculoDAO;
import br.com.Locadora.model.FixedLengthJTextField;


public class TelaConsultaVeiculo extends JDialog {

	private static final long serialVersionUID = -7616001911230736284L;

	private JPanel contentPane;
	private JPanel panelTable;
	private JLabel labelCodigo;
	private JLabel labelNome;
	private JTextField fieldCodigo;
	private JFormattedTextField formattedTextFieldPlaca;
	private JButton buttonResearch;
	private JButton buttonSelect;
	private JTable tableEmpresas;
	private JSeparator separator;
	private JScrollPane scrollPaneTable;
	private DefaultTableModel modelTalble; 

	private VeiculoDAO veiculoDAO;
	private JButton buttonLimpar;
	
	private TelaCadastroVeiculoOK telaVeiculo;
	private TelaLocacao telaLocacao;
	
	private ButtonHandlerConsultaVeiculos buttonHandlerConsultaVeiculos;
	
	@SuppressWarnings("serial")
	public TelaConsultaVeiculo(Object objectTela) {
		setResizable(false);
		setTitle("Consulta Ve�culos");
		setType(Type.POPUP);
		setModal(true);
		setAlwaysOnTop(true);
		setSize(600, 315);
		setLocationRelativeTo(null);
		
		buttonHandlerConsultaVeiculos = new ButtonHandlerConsultaVeiculos(this, objectTela, telaVeiculo, telaLocacao);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		labelCodigo = new JLabel("C�digo:");
		labelCodigo.setBounds(10, 15, 49, 14);
		contentPane.add(labelCodigo);

		fieldCodigo = new JTextField(10);
		fieldCodigo.setDocument(new FixedLengthJTextField(15));
		fieldCodigo.setBounds(58, 12, 58, 20);
		contentPane.add(fieldCodigo);
		fieldCodigo.setColumns(10);

		labelNome = new JLabel("Placa:");
		labelNome.setBounds(128, 15, 40, 14);
		contentPane.add(labelNome);

		formattedTextFieldPlaca = new JFormattedTextField(Mascara("UUU-####"));
		formattedTextFieldPlaca.setBounds(170, 12, 92, 20);
		contentPane.add(formattedTextFieldPlaca);
		formattedTextFieldPlaca.setColumns(10);

		veiculoDAO = new VeiculoDAO();
		
		buttonResearch = new JButton("Pesquisar");
		buttonResearch.addActionListener(buttonHandlerConsultaVeiculos);
		buttonResearch.setBounds(392, 11, 92, 23);
		contentPane.add(buttonResearch);

		buttonSelect = new JButton("Selecionar");
		buttonSelect.setBounds(484, 11, 98, 23);
		buttonSelect.setEnabled(false);
		buttonSelect.addActionListener(buttonHandlerConsultaVeiculos);
		contentPane.add(buttonSelect);

		separator = new JSeparator();
		separator.setBounds(10, 41, 572, 2);
		contentPane.add(separator);

		panelTable = new JPanel();
		panelTable.setBounds(10, 49, 572, 217);
		contentPane.add(panelTable);
		panelTable.setLayout(null);

		scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBounds(0, 0, 572, 216);
		panelTable.add(scrollPaneTable);

		modelTalble = new DefaultTableModel(null,   
				new String [] {"C�DIGO","CHASSI", "PLACA", "TIPO", "CATEGORIA", "ANO MODELO", "ANO FABRICA��O"}){      

			boolean[] canEdit = new boolean []{false, false, false, false, false, false, false};        

			@Override  
			public boolean isCellEditable(int rowIndex, int columnIndex) {        
				return canEdit [columnIndex];        
			}      
		};    

		tableEmpresas = new JTable(modelTalble);
		tableEmpresas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPaneTable.setViewportView(tableEmpresas);
		
		buttonLimpar = new JButton("Limpar");
		buttonLimpar.addActionListener(buttonHandlerConsultaVeiculos);
		buttonLimpar.setBounds(300, 11, 92, 23);
		contentPane.add(buttonLimpar);
		
		tableEmpresas.getColumnModel().getColumn(0).setPreferredWidth(50);
		tableEmpresas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tableEmpresas.getColumnModel().getColumn(2).setPreferredWidth(70);
		tableEmpresas.getColumnModel().getColumn(3).setPreferredWidth(100);
		tableEmpresas.getColumnModel().getColumn(4).setPreferredWidth(150);
		tableEmpresas.getColumnModel().getColumn(5).setPreferredWidth(150);

	}

	private MaskFormatter Mascara(String mascara){
		MaskFormatter formatter;

		try {
			formatter = new MaskFormatter();
			formatter.setMask(mascara);
			return formatter;
		} catch (Exception e) {
			return null;
		}
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JPanel getPanelTable() {
		return panelTable;
	}

	public JLabel getLabelCodigo() {
		return labelCodigo;
	}

	public JLabel getLabelNome() {
		return labelNome;
	}

	public JTextField getFieldCodigo() {
		return fieldCodigo;
	}

	public JFormattedTextField getFormattedTextFieldPlaca() {
		return formattedTextFieldPlaca;
	}

	public JButton getButtonResearch() {
		return buttonResearch;
	}

	public JButton getButtonSelect() {
		return buttonSelect;
	}

	public JTable getTableEmpresas() {
		return tableEmpresas;
	}

	public JSeparator getSeparator() {
		return separator;
	}

	public JScrollPane getScrollPaneTable() {
		return scrollPaneTable;
	}

	public DefaultTableModel getModelTalble() {
		return modelTalble;
	}

	public VeiculoDAO getVeiculoDAO() {
		return veiculoDAO;
	}

	public ButtonHandlerConsultaVeiculos getButtonHandlerConsultaVeiculos() {
		return buttonHandlerConsultaVeiculos;
	}

	public JButton getButtonLimpar() {
		return buttonLimpar;
	}

	public TelaCadastroVeiculoOK getTelaVeiculo() {
		return telaVeiculo;
	}

	public TelaLocacao getTelaLocacao() {
		return telaLocacao;
	}
}
