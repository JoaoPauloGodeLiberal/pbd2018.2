package br.com.Locadora.view;


import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import br.com.Locadora.controller.ButtonHandlerTelaManutencaoPreco;
import br.com.Locadora.dao.TiposLocacaoDAO;
import br.com.Locadora.model.TiposLocacao;


public class TelaManutencaoPreco extends JInternalFrame {


	private static final long serialVersionUID = -2513465114625279076L;
	
	private JPanel contentPane;
	private JPanel panelTitulo;
	private JPanel panelCentro;
	private JPanel panelRodape;
	private JLabel labelTitulo;
	private JLabel labelTipos;
	private JLabel labelDescricao;
	private JFormattedTextField fieldPrecoKml;
	private JTextField fieldKml;
	private JTextField fieldKmC;
	private JFormattedTextField textFieldPrecoKmc;
	private JButton buttonSalvar;
	private JButton buttonCancelar;

	private TiposLocacaoDAO tiposLocacaoDAO;
	private List<TiposLocacao> tiposLocacaos;
	
	private ButtonHandlerTelaManutencaoPreco buttonHandlerTelaManutencaoPreco;

	public TelaManutencaoPreco() {
		setTitle("Manuten��o de Pre�os");
		setClosable(true);
		setResizable(false);
		setBounds(100, 100, 439, 279);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		tiposLocacaoDAO = new TiposLocacaoDAO();
		tiposLocacaos = tiposLocacaoDAO.Consulta();
		
		buttonHandlerTelaManutencaoPreco = new ButtonHandlerTelaManutencaoPreco(this);

		panelTitulo = new JPanel();
		panelTitulo.setBounds(0, 0, 430, 57);
		contentPane.add(panelTitulo);

		labelTitulo = new JLabel("Manuten��o de Pre�os");
		panelTitulo.add(labelTitulo);

		panelCentro = new JPanel();
		panelCentro.setBounds(12, 104, 405, 101);
		contentPane.add(panelCentro);
		panelCentro.setLayout(null);

		labelDescricao = new JLabel("Pre�os:");
		labelDescricao.setBounds(64, 51, 44, 16);
		panelCentro.add(labelDescricao);

		fieldPrecoKml = new JFormattedTextField();
		DecimalFormat dFormat1 = new DecimalFormat("####.00");
		dFormat1.setMaximumIntegerDigits(4);
		NumberFormatter Formatter1 = new NumberFormatter(dFormat1);
		Formatter1.setFormat (dFormat1);
		Formatter1.setAllowsInvalid (false);
		
		fieldPrecoKml.setFormatterFactory(new DefaultFormatterFactory(Formatter1));
		fieldPrecoKml.setText(String.valueOf(tiposLocacaos.get(0).getValor()));
		fieldPrecoKml.setBounds(111, 49, 97, 20);
		panelCentro.add(fieldPrecoKml);

		labelTipos = new JLabel("Tipos:");
		labelTipos.setBounds(74, 26, 34, 16);
		panelCentro.add(labelTipos);

		fieldKml = new JTextField(tiposLocacaos.get(0).getTipo());
		fieldKml.setEditable(false);
		fieldKml.setBounds(111, 24, 97, 20);
		panelCentro.add(fieldKml);
		fieldKml.setColumns(10);

		fieldKmC = new JTextField(tiposLocacaos.get(1).getTipo());
		fieldKmC.setEditable(false);
		fieldKmC.setBounds(220, 24, 97, 20);
		panelCentro.add(fieldKmC);
		fieldKmC.setColumns(10);

		textFieldPrecoKmc = new JFormattedTextField();
		DecimalFormat dFormat = new DecimalFormat("####.00");
		dFormat.setMaximumIntegerDigits(4);
		NumberFormatter Formatter = new NumberFormatter(dFormat);
		Formatter.setFormat (dFormat);
		Formatter.setAllowsInvalid (false);
		
		textFieldPrecoKmc.setFormatterFactory(new DefaultFormatterFactory(Formatter));
		textFieldPrecoKmc.setText(String.valueOf(tiposLocacaos.get(1).getValor()));
		textFieldPrecoKmc.setColumns(10);
		textFieldPrecoKmc.setBounds(220, 49, 97, 20);
		panelCentro.add(textFieldPrecoKmc);

		panelRodape = new JPanel();
		panelRodape.setBounds(0, 232, 430, 17);
		contentPane.add(panelRodape);

		buttonSalvar = new JButton("Salvar");
		buttonSalvar.setToolTipText("Salvar");
		buttonSalvar.setBounds(125, 66,70, 26);
		contentPane.add(buttonSalvar);

		buttonCancelar = new JButton("Desfazer");
		buttonCancelar.setToolTipText("Desfazer");
		buttonCancelar.setBounds(249, 66, 85, 26);
		contentPane.add(buttonCancelar);

		buttonSalvar.addActionListener(buttonHandlerTelaManutencaoPreco);
		buttonCancelar.addActionListener(buttonHandlerTelaManutencaoPreco);

	}


	public boolean validarFields(){
		if (fieldPrecoKml.getText().isEmpty()||fieldKml.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Campos Obrigat�rios", null, JOptionPane.WARNING_MESSAGE);
			return false;
		}

		return true;
	}

	public void setPosicao() {
		Dimension d = this.getDesktopPane().getSize();
		this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2); 
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public JPanel getContentPane() {
		return contentPane;
	}


	public JPanel getPanelTitulo() {
		return panelTitulo;
	}


	public JPanel getPanelCentro() {
		return panelCentro;
	}


	public JPanel getPanelRodape() {
		return panelRodape;
	}


	public JLabel getLabelTitulo() {
		return labelTitulo;
	}


	public JLabel getLabelTipos() {
		return labelTipos;
	}


	public JLabel getLabelDescricao() {
		return labelDescricao;
	}


	public JFormattedTextField getFieldPrecoKml() {
		return fieldPrecoKml;
	}


	public JTextField getFieldKml() {
		return fieldKml;
	}


	public JTextField getFieldKmC() {
		return fieldKmC;
	}


	public JFormattedTextField getTextFieldPrecoKmc() {
		return textFieldPrecoKmc;
	}


	public JButton getButtonSalvar() {
		return buttonSalvar;
	}


	public JButton getButtonCancelar() {
		return buttonCancelar;
	}


	public TiposLocacaoDAO getTiposLocacaoDAO() {
		return tiposLocacaoDAO;
	}


	public List<TiposLocacao> getTiposLocacaos() {
		return tiposLocacaos;
	}


	public ButtonHandlerTelaManutencaoPreco getButtonHandlerTelaManutencaoPreco() {
		return buttonHandlerTelaManutencaoPreco;
	}
}
