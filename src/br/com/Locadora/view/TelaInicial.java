package br.com.Locadora.view;

import java.awt.Color;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import br.com.Locadora.controller.ButtonHandlerTelaInicial;
import br.com.Locadora.model.Usuario;

public class TelaInicial extends JFrame {

	private static final long serialVersionUID = -2773811355479248366L;

	private JPanel contentPane;
	private JPanel panelInformacoes;
	private JDesktopPane desktopPane;
	private JMenuBar menuBar;
	private JMenu menuArquivo;
	private JMenu menuCadastros;
	private JMenu menuReservas;
	private JMenu menuLocao;
	private JMenu menuRelatorios;
	private JMenu menuSobre;
	private JMenu menuRCadastro;
	private JMenuItem mnitemVeiculos;
	private JMenuItem mnitemClientes;
	private JMenuItem mnitemEmpresas;
	private JMenuItem mnitemUsuarios;
	private JMenuItem mnitemSair;
	private JMenuItem mnitemSobreOSistema;
	private JMenuItem mnitemRClientes;
	private JMenuItem mnitemRClientesPessoaFsica;
	private JMenuItem mnitemRClientesPessoaJurdica;
	private JMenuItem mnitemREmpresas;
	private JMenuItem mnitemRUsuarios;
	private JMenuItem mnitemRVeiculos;
	private JLabel labelBackground;
	private JLabel labelInformacoes;
	private DateFormat format;

	private Usuario usuarioLogado;

	private TelaCadastroClienteOK cadastroCliente;
	private TelaCadastroEmpresaOK cadastroEmpresa;
	private TelaCadastroVeiculoOK cadastroVeiculo;
	private TelaCadastroUsuarioOK cadastroUsuario;
	private TelaCadastroCategoriaOK cadastroCategoria;
	private TelaReserva telaReserva;
	private TelaLocacao telaLocacao;
	private TelaManutencaoPreco manutencaoPreco;

	private GerarRelatorio gerarRelatorio;
	private JMenuItem mnitemCategorias;
	private JMenuItem mnitemManutencaoDeLocacao;
	private JMenuItem mnitemManutenoDePrecos;
	private JMenuItem mnitemManutencaoDeReservas;

	private ButtonHandlerTelaInicial buttonHandlerTelaInicial;

	public TelaInicial(Usuario usuarioLogado) {
		setTitle("Tela Inicial");		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(Toolkit.getDefaultToolkit().getScreenSize());
		setExtendedState(MAXIMIZED_BOTH);

		buttonHandlerTelaInicial = new ButtonHandlerTelaInicial(this, cadastroCliente, cadastroVeiculo, cadastroEmpresa, cadastroCategoria, cadastroUsuario, telaReserva, telaLocacao, manutencaoPreco, gerarRelatorio);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		desktopPane = new JDesktopPane();
		desktopPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		desktopPane.setBounds(1, 0, 1365, 663);
		contentPane.add(desktopPane);

		labelBackground = new JLabel();
		labelBackground.setBounds(0, 0, 1365, 660);
		desktopPane.setBackground(Color.BLACK);
		desktopPane.add(labelBackground);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		menuArquivo = new JMenu("Logoff");
		menuBar.add(menuArquivo);

		mnitemSair = new JMenuItem("Sair");
		mnitemSair.addActionListener(buttonHandlerTelaInicial);
		menuArquivo.add(mnitemSair);

		menuCadastros = new JMenu("Cadastrar");
		menuBar.add(menuCadastros);

		mnitemClientes = new JMenuItem("Cliente");
		mnitemClientes.addActionListener(buttonHandlerTelaInicial);
		
		mnitemEmpresas = new JMenuItem("Empresa");
		mnitemEmpresas.addActionListener(buttonHandlerTelaInicial);
		mnitemVeiculos = new JMenuItem("Ve�culo");
		
		mnitemVeiculos.addActionListener(buttonHandlerTelaInicial);

		mnitemCategorias = new JMenuItem("Categoria");
		mnitemCategorias.addActionListener(buttonHandlerTelaInicial);
		menuCadastros.add(mnitemCategorias);

		menuCadastros.add(mnitemClientes);
		menuCadastros.add(mnitemEmpresas);
		mnitemUsuarios = new JMenuItem("Usu�rio");
		mnitemUsuarios.addActionListener(buttonHandlerTelaInicial);
		
		menuCadastros.add(mnitemUsuarios);
		menuCadastros.add(mnitemVeiculos);

		menuReservas = new JMenu("Reservas");
		menuBar.add(menuReservas);

		mnitemManutencaoDeReservas = new JMenuItem("Realizar de Reservas");
		mnitemManutencaoDeReservas.addActionListener(buttonHandlerTelaInicial);
		menuReservas.add(mnitemManutencaoDeReservas);

		menuLocao = new JMenu("Loca��o");
		menuBar.add(menuLocao);

		mnitemManutencaoDeLocacao = new JMenuItem("Realizar Loca��o");
		mnitemManutencaoDeLocacao.addActionListener(buttonHandlerTelaInicial);
		menuLocao.add(mnitemManutencaoDeLocacao);

		mnitemManutenoDePrecos = new JMenuItem("Alterar Pre�os");
		mnitemManutenoDePrecos.addActionListener(buttonHandlerTelaInicial);
		menuLocao.add(mnitemManutenoDePrecos);
		// INICIO SUBMENU RELAT�RIOS		
		menuRelatorios = new JMenu("Relat�rios");
		menuBar.add(menuRelatorios);

		menuRCadastro = new JMenu("Cadastro");

		mnitemRClientes = new JMenuItem("Cliente");
		mnitemRClientes.addActionListener(buttonHandlerTelaInicial);
		menuRCadastro.add(mnitemRClientes);

		mnitemRClientesPessoaFsica = new JMenuItem("Cliente Pessoa F�sica");
		
		mnitemRClientesPessoaFsica.addActionListener(buttonHandlerTelaInicial);
		
		menuRCadastro.add(mnitemRClientesPessoaFsica);

		mnitemRClientesPessoaJurdica = new JMenuItem("Clientes Pessoa Jur�dica");
		
		mnitemRClientesPessoaJurdica.addActionListener(buttonHandlerTelaInicial);
		
		menuRCadastro.add(mnitemRClientesPessoaJurdica);

		mnitemREmpresas = new JMenuItem("Empresas");
		mnitemREmpresas.addActionListener(buttonHandlerTelaInicial);
		menuRCadastro.add(mnitemREmpresas);

		mnitemRUsuarios = new JMenuItem("Usu�rios");
		mnitemRUsuarios.addActionListener(buttonHandlerTelaInicial);
		menuRCadastro.add(mnitemRUsuarios);

		mnitemRVeiculos = new JMenuItem("Ve�culos");
		mnitemRVeiculos.addActionListener(buttonHandlerTelaInicial);
		menuRCadastro.add(mnitemRVeiculos);
		//	FIM SUBMENU RELAT�RIOS			

		menuRelatorios.add(menuRCadastro);

		menuSobre = new JMenu("Sobre");
		menuBar.add(menuSobre);

		mnitemSobreOSistema = new JMenuItem("Sobre o Sistema");
		mnitemSobreOSistema.addActionListener(buttonHandlerTelaInicial);
		menuSobre.add(mnitemSobreOSistema);

		panelInformacoes = new JPanel();
		panelInformacoes.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelInformacoes.setBounds(0, 662, 1370, 23);
		contentPane.add(panelInformacoes);
		panelInformacoes.setLayout(null);

		this.usuarioLogado = usuarioLogado;
		format = new SimpleDateFormat("dd/MM/YYYY");
		labelInformacoes = new JLabel("Usu�rio: "+usuarioLogado.getLogin().toUpperCase()+"    Empresa: "+usuarioLogado.getEmpresa().getNome().toUpperCase()+"    Data: "+format.format(new Date()));
		labelInformacoes.setBounds(3, 4, 558, 14);
		panelInformacoes.add(labelInformacoes);

	}

	public JMenuItem getMenuitemVeiculos() {
		return mnitemVeiculos;
	}

	public JMenuItem getMenuitemClientes() {
		return mnitemClientes;
	}

	public JMenuItem getMenuitemEmpresas() {
		return mnitemEmpresas;
	}

	public JMenu getMenuRCadastro() {
		return menuRCadastro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JPanel getPanelInformacoes() {
		return panelInformacoes;
	}

	public JDesktopPane getDesktopPane() {
		return desktopPane;
	}

//	public JMenuBar getMenuBar() {
//		return menuBar;
//	}

	public JMenu getMenuArquivo() {
		return menuArquivo;
	}

	public JMenu getMenuCadastros() {
		return menuCadastros;
	}

	public JMenu getMenuReservas() {
		return menuReservas;
	}

	public JMenu getMenuLocao() {
		return menuLocao;
	}

	public JMenu getMenuRelatorios() {
		return menuRelatorios;
	}

	public JMenu getMenuSobre() {
		return menuSobre;
	}

	public JMenuItem getMnitemVeiculos() {
		return mnitemVeiculos;
	}

	public JMenuItem getMnitemClientes() {
		return mnitemClientes;
	}

	public JMenuItem getMnitemEmpresas() {
		return mnitemEmpresas;
	}

	public JMenuItem getMnitemUsuarios() {
		return mnitemUsuarios;
	}

	public JMenuItem getMnitemSair() {
		return mnitemSair;
	}

	public JMenuItem getMnitemSobreOSistema() {
		return mnitemSobreOSistema;
	}

	public JMenuItem getMnitemRClientes() {
		return mnitemRClientes;
	}

	public JMenuItem getMnitemRClientesPessoaFsica() {
		return mnitemRClientesPessoaFsica;
	}

	public JMenuItem getMnitemRClientesPessoaJurdica() {
		return mnitemRClientesPessoaJurdica;
	}

	public JMenuItem getMnitemREmpresas() {
		return mnitemREmpresas;
	}

	public JMenuItem getMnitemRUsuarios() {
		return mnitemRUsuarios;
	}

	public JMenuItem getMnitemRVeiculos() {
		return mnitemRVeiculos;
	}

	public JLabel getLabelBackground() {
		return labelBackground;
	}

	public JLabel getLabelInformacoes() {
		return labelInformacoes;
	}

	public DateFormat getFormat() {
		return format;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public TelaCadastroClienteOK getCadastroCliente() {
		return cadastroCliente;
	}

	public TelaCadastroEmpresaOK getCadastroEmpresa() {
		return cadastroEmpresa;
	}

	public TelaCadastroVeiculoOK getCadastroVeiculo() {
		return cadastroVeiculo;
	}

	public TelaCadastroUsuarioOK getCadastroUsuario() {
		return cadastroUsuario;
	}

	public TelaCadastroCategoriaOK getCadastroCategoria() {
		return cadastroCategoria;
	}

	public TelaReserva getTelaReserva() {
		return telaReserva;
	}

	public TelaLocacao getTelaLocacao() {
		return telaLocacao;
	}

	public TelaManutencaoPreco getManutencaoPreco() {
		return manutencaoPreco;
	}

	public GerarRelatorio getGerarRelatorio() {
		return gerarRelatorio;
	}

	public JMenuItem getMnitemCategorias() {
		return mnitemCategorias;
	}

	public JMenuItem getMnitemManutencaoDeLocacao() {
		return mnitemManutencaoDeLocacao;
	}

	public JMenuItem getMnitemManutenoDePrecos() {
		return mnitemManutenoDePrecos;
	}

	public JMenuItem getMnitemManutencaoDeReservas() {
		return mnitemManutencaoDeReservas;
	}

	public ButtonHandlerTelaInicial getButtonHandlerTelaInicial() {
		return buttonHandlerTelaInicial;
	}

	public void setCadastroCliente(TelaCadastroClienteOK cadastroCliente) {
		this.cadastroCliente = cadastroCliente;
	}

	public boolean isCreate(JInternalFrame object){
		if(object == null)
			return true;
		else
			return false;
	}
}
