package br.com.Locadora.view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import br.com.Locadora.controller.ButtonhandlerTelaLogin;
import br.com.Locadora.model.FixedLengthJTextField;

public class TelaLogin extends JFrame{

	private static final long serialVersionUID = -7148671693491049508L;
	public static final int ERROR_CONNECT_DB = 1;
	public static final int LOGIN_INVALID = 2;
	public static final int FIELDS_EMPTY = 3;
	
	private JPanel contentPane;
	private JLabel labelBackground;
	private JPasswordField passwordField;
	private JTextField FieldUser;
	private JButton buttonEntrar;
	private JButton esqueciSenhaButton;
	
	private ButtonhandlerTelaLogin buttonhandlerTelaLogin;
	
	public TelaLogin() {
		
		buttonhandlerTelaLogin = new ButtonhandlerTelaLogin(this);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);

		labelBackground = new JLabel();
		labelBackground.setIcon(new ImageIcon("imagens/login.png"));
		labelBackground.setBounds(0, 0, 528, 290);

		FieldUser = new JTextField();
		FieldUser.setForeground(Color.CYAN);
		FieldUser.setBackground(Color.BLACK);
		FieldUser.setFont(new Font("Tahoma", Font.BOLD, 20));
		FieldUser.setToolTipText("Usu�rio");
		FieldUser.setBounds(230, 88, 148, 29);
		
		passwordField = new JPasswordField();
		passwordField.setToolTipText("Senha");
		passwordField.setDocument(new FixedLengthJTextField(6));
		passwordField.setFont(new Font("Tahoma", Font.BOLD, 20));
		passwordField.setBackground(Color.BLACK);
		passwordField.setForeground(Color.CYAN);
		passwordField.setBounds(230, 169, 148, 29);
		
		buttonEntrar = new JButton("");
		buttonEntrar.setIcon(new ImageIcon("imagens/entrarButton.jpg"));
		buttonEntrar.setBounds(394, 240, 124, 38);
		
		esqueciSenhaButton = new JButton("");
		esqueciSenhaButton.setIcon(new ImageIcon("imagens/resetButton.jpg"));
		esqueciSenhaButton.setBounds(10, 225, 120, 51);
		
		getEsqueciSenhaButton().addActionListener(buttonhandlerTelaLogin);
		getButtonEntrar().addActionListener(buttonhandlerTelaLogin);
		getFieldUser().addKeyListener(buttonhandlerTelaLogin.enter());
		getPasswordField().addKeyListener(buttonhandlerTelaLogin.validarsenha());
		
		contentPane.add(FieldUser);
		contentPane.add(passwordField);
		contentPane.add(buttonEntrar);
		contentPane.add(esqueciSenhaButton);
		contentPane.add(labelBackground);
		
		setTitle("Tela de Login");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(534, 318);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public JTextField getFieldUser() {
		return FieldUser;
	}

	public JButton getButtonEntrar() {
		return buttonEntrar;
	}
	
	public JButton getEsqueciSenhaButton() {
		return esqueciSenhaButton;
	}

	public void showMensseger(int typeMenssage){
		switch (typeMenssage) {
		case ERROR_CONNECT_DB:{JOptionPane.showMessageDialog(null, "Erro ao Connectar o Banco de Dados.\n        O Sistema ser� Finalizado\n","Erro de Conex�o",JOptionPane.ERROR_MESSAGE);} break;
		case LOGIN_INVALID:{JOptionPane.showMessageDialog(null, "Login Inv�lido",null ,JOptionPane.WARNING_MESSAGE);}break;
		case FIELDS_EMPTY:{JOptionPane.showMessageDialog(null,"Campos Obrigat�rios",null,JOptionPane.INFORMATION_MESSAGE);} break;
		}
	}
}
