package br.com.Locadora.view;


import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import br.com.Locadora.controller.ButtonHandlerTelaLocacao;
import br.com.Locadora.dao.EmpresaDAO;
import br.com.Locadora.dao.LocacaoDAO;
import br.com.Locadora.model.Cliente;
import br.com.Locadora.model.Empresa;
import br.com.Locadora.model.Locacao;
import br.com.Locadora.model.PessoaFisica;
import br.com.Locadora.model.Reserva;
import br.com.Locadora.model.TiposLocacao;
import br.com.Locadora.model.Veiculo;


public class TelaLocacao extends JInternalFrame {


	private static final long serialVersionUID = 3691713444891118750L;

	private JPanel contentPane;
	private JPanel panelCentro;
	private JLabel labelCodigo;
	private JLabel labelData;
	private JLabel labelStatus;
	private JLabel labelEmpresaDevolucao;
	private JLabel labelMotorista;
	private JLabel labelCliente;
	private JLabel labelDataDeRetirada;
	private JLabel labelValor;
	private JLabel lblTipo;
	private JLabel labelRS;
	private JLabel labelKms;
	private JLabel lblVeculo;
	private JTextField fieldCodigo;
	private JTextField fieldCliente;
	private JTextField fieldData;
	private JTextField fieldStatus;
	private JTextField fieldMotorista;
	private JTextField fieldValor;
	private JTextField fieldKms;
	private JTextField fieldVeiculo;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxEmpresaDevolucao;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxTipoLocacao;
	private JCheckBox chckbxCancelarLocacao;
	private JButton buttonSalvar;
	private JButton buttonCancelar;
	private JButton buttonExcluir;
	private JButton buttonLocalizar;
	private JButton buttonNovo;
	private JButton buttonLocalizarMotorista;
	private JButton buttonLocalizaVeiculo;
	private JButton buttonLocalizaCliente;
	private JDateChooser dateChooserDataDevolucao;
	
	private boolean saveupdate;
	private double valorTotal;
	private Date dataAtual;
	
	private LocacaoDAO locacaoDAO;
	private EmpresaDAO empresaDAO;
	
	private Locacao locacaoUpdate;
	private List<Empresa> empresas;
	private List<TiposLocacao> tiposLocacao;
	private Cliente cliente;
	private PessoaFisica motorista;
	private Veiculo veiculo;
	private Reserva reservaImportada;
	private ButtonHandlerTelaLocacao buttonHandlerTelaLocacao;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TelaLocacao() {
		setTitle("Loca��o");
		setClosable(true);
		setResizable(false);
		setBounds(100, 100, 635, 298);
		
		buttonHandlerTelaLocacao = new ButtonHandlerTelaLocacao(this);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		locacaoDAO = new LocacaoDAO();
		empresaDAO = new EmpresaDAO();
		
		dataAtual = new Date();

		panelCentro = new JPanel();
		panelCentro.setBounds(10, 11, 603, 204);
		contentPane.add(panelCentro);
		panelCentro.setLayout(null);

		labelData = new JLabel("Data:");
		labelData.setBounds(164, 15, 29, 16);
		panelCentro.add(labelData);
		labelData.setFont(new Font("SansSerif", Font.BOLD, 12));

		labelStatus = new JLabel("Status:");
		labelStatus.setBounds(281, 15, 45, 16);
		panelCentro.add(labelStatus);
		labelStatus.setFont(new Font("SansSerif", Font.BOLD, 12));

		labelEmpresaDevolucao = new JLabel("Empresa Devolu��o:");
		labelEmpresaDevolucao.setBounds(15, 127, 116, 16);
		panelCentro.add(labelEmpresaDevolucao);
		labelEmpresaDevolucao.setFont(new Font("SansSerif", Font.BOLD, 12));

		comboBoxEmpresaDevolucao = new JComboBox();
		empresas = empresaDAO.listALL();
		for(int i=0; i <empresas.size(); i++) {
			comboBoxEmpresaDevolucao.addItem(empresas.get(i).getId()+" - "+empresas.get(i).getNome());
		}
		comboBoxEmpresaDevolucao.setEnabled(false);
		comboBoxEmpresaDevolucao.setBounds(135, 125, 147, 20);
		panelCentro.add(comboBoxEmpresaDevolucao);

		labelCliente = new JLabel("Cliente:");
		labelCliente.setBounds(30, 43, 42, 16);
		panelCentro.add(labelCliente);
		labelCliente.setFont(new Font("SansSerif", Font.BOLD, 12));

		fieldCliente = new JTextField();
		fieldCliente.setEditable(false);
		fieldCliente.setBounds(82, 41, 306, 20);
		panelCentro.add(fieldCliente);
		fieldCliente.setColumns(10);

		fieldData = new JTextField();
		fieldData.setEditable(false);
		fieldData.setBounds(197, 13, 72, 20);
		panelCentro.add(fieldData);
		fieldData.setColumns(10);

		labelCodigo = new JLabel("C�digo:");
		labelCodigo.setBounds(30, 15, 42, 16);
		panelCentro.add(labelCodigo);

		fieldCodigo = new JTextField();
		fieldCodigo.setEditable(false);
		fieldCodigo.setBounds(82, 13, 64, 20);
		panelCentro.add(fieldCodigo);
		fieldCodigo.setColumns(10);
		
		fieldStatus = new JTextField();
		fieldStatus.setEditable(false);
		fieldStatus.setBounds(325, 13, 130, 20);
		panelCentro.add(fieldStatus);
		fieldStatus.setColumns(10);
		
		labelMotorista = new JLabel("Motorista:");
		labelMotorista.setBounds(18, 73, 57, 16);
		panelCentro.add(labelMotorista);
		
		fieldMotorista = new JTextField();
		fieldMotorista.setEditable(false);
		fieldMotorista.setBounds(82, 71, 306, 20);
		panelCentro.add(fieldMotorista);
		
		buttonLocalizarMotorista = new JButton();
		buttonLocalizarMotorista.setText("Pesquisar");
		buttonLocalizarMotorista.setEnabled(false);
		buttonLocalizarMotorista.setToolTipText("Pesquisar");
		buttonLocalizarMotorista.setBounds(393, 68, 92, 26);
		panelCentro.add(buttonLocalizarMotorista);
		
		labelDataDeRetirada = new JLabel("Data de Devolu��o:");
		labelDataDeRetirada.setBounds(310, 127, 108, 16);
		panelCentro.add(labelDataDeRetirada);
		
		dateChooserDataDevolucao = new JDateChooser(dataAtual);
		dateChooserDataDevolucao.setEnabled(false);
		dateChooserDataDevolucao.setBounds(423, 125, 92, 20);
		panelCentro.add(dateChooserDataDevolucao);
		
		chckbxCancelarLocacao = new JCheckBox("Cancelar Loca��o");
		chckbxCancelarLocacao.setEnabled(false);
		chckbxCancelarLocacao.setBounds(465, 11, 128, 24);
		panelCentro.add(chckbxCancelarLocacao);
		
		labelValor = new JLabel("Valor:");
		labelValor.setFont(new Font("Dialog", Font.BOLD, 16));
		labelValor.setBounds(439, 161, 46, 21);
		panelCentro.add(labelValor);
		
		lblTipo = new JLabel("Tipo:");
		lblTipo.setBounds(45, 153, 27, 16);
		panelCentro.add(lblTipo);
		
		comboBoxTipoLocacao = new JComboBox();
		tiposLocacao = locacaoDAO.listTipoLocacao();
		for(int i=0; i <tiposLocacao.size(); i++) {
			comboBoxTipoLocacao.addItem(tiposLocacao.get(i).getTipo());
		}
		comboBoxTipoLocacao.setEnabled(false);
		comboBoxTipoLocacao.setBounds(82, 151, 98, 20);
		panelCentro.add(comboBoxTipoLocacao);
		
		fieldValor = new JTextField();
		fieldValor.setFont(new Font("Dialog", Font.BOLD, 14));
		fieldValor.setEditable(false);
		fieldValor.setBounds(486, 161, 86, 21);
		panelCentro.add(fieldValor);
		fieldValor.setColumns(10);
		
		labelRS = new JLabel("R$");
		labelRS.setFont(new Font("Dialog", Font.BOLD, 16));
		labelRS.setBounds(572, 161, 21, 21);
		panelCentro.add(labelRS);
		
		labelKms = new JLabel("Quantidade de Km's:");
		labelKms.setBounds(197, 153, 117, 16);
		panelCentro.add(labelKms);
		
		fieldKms = new JTextField();
		fieldKms.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				atualizaValor();
			}
		});
		fieldKms.setEditable(false);
		fieldKms.setBounds(316, 151, 57, 20);
		panelCentro.add(fieldKms);
		fieldKms.setColumns(10);
		
		lblVeculo = new JLabel("Ve�culo:");
		lblVeculo.setBounds(30, 101, 45, 16);
		panelCentro.add(lblVeculo);
		
		fieldVeiculo = new JTextField();
		fieldVeiculo.setEditable(false);
		fieldVeiculo.setBounds(82, 99, 306, 20);
		panelCentro.add(fieldVeiculo);
		fieldVeiculo.setColumns(10);
		
		buttonLocalizaVeiculo = new JButton("Pesquisar");
		buttonLocalizaVeiculo.setToolTipText("Pesquisar");
		buttonLocalizaVeiculo.setEnabled(false);
		buttonLocalizaVeiculo.setBackground(SystemColor.inactiveCaption);
		buttonLocalizaVeiculo.setBounds(393, 96, 92, 26);
		panelCentro.add(buttonLocalizaVeiculo);
		
		buttonLocalizaCliente = new JButton("Pesquisar");
		buttonLocalizaCliente.setToolTipText("Pesquisar");
		buttonLocalizaCliente.setEnabled(false);
		buttonLocalizaCliente.setBounds(393, 38, 92, 26);
		panelCentro.add(buttonLocalizaCliente);

		buttonNovo = new JButton("Novo");
		buttonNovo.setToolTipText("Novo");
		buttonNovo.setBounds(10, 226, 71, 26);
		contentPane.add(buttonNovo);
		
		buttonSalvar = new JButton("Salvar");
		buttonSalvar.setToolTipText("Gravar");
		buttonSalvar.setEnabled(false);
		buttonSalvar.setBounds(91, 226, 63, 26);
		contentPane.add(buttonSalvar);
		
		buttonExcluir = new JButton("Excluir");
		buttonExcluir.setToolTipText("Excluir");
		buttonExcluir.setEnabled(false);
		buttonExcluir.setBounds(164, 226, 71, 26);
		contentPane.add(buttonExcluir);
		
		buttonCancelar = new JButton("Desfazer");
		buttonCancelar.setToolTipText("Desfazer");
		buttonCancelar.setBounds(245, 226, 89, 26);
		contentPane.add(buttonCancelar);
		
		buttonLocalizar = new JButton("Localizar");
		buttonLocalizar.setToolTipText("Localizar");
		buttonLocalizar.setBounds(344, 226, 73, 26);
		contentPane.add(buttonLocalizar);
		
		buttonNovo.addActionListener(buttonHandlerTelaLocacao);
		buttonSalvar.addActionListener(buttonHandlerTelaLocacao);
		buttonExcluir.addActionListener(buttonHandlerTelaLocacao);
		buttonCancelar.addActionListener(buttonHandlerTelaLocacao);
		buttonLocalizar.addActionListener(buttonHandlerTelaLocacao);
		buttonLocalizarMotorista.addActionListener(buttonHandlerTelaLocacao);
		buttonLocalizaCliente.addActionListener(buttonHandlerTelaLocacao);
		buttonLocalizaVeiculo.addActionListener(buttonHandlerTelaLocacao);
		comboBoxTipoLocacao.addActionListener(buttonHandlerTelaLocacao);
		
		setVisible(true);
	}

	public boolean validarFields(){
		if (fieldCliente.getText().isEmpty()||fieldMotorista.getText().isEmpty()||fieldVeiculo.getText().isEmpty()||(comboBoxTipoLocacao.getSelectedIndex()==1? fieldKms.getText().isEmpty(): false)) {
			JOptionPane.showMessageDialog(null, "Campos Obrigat�rios", null, JOptionPane.WARNING_MESSAGE);
			return false;
		}else if (dateChooserDataDevolucao.getDate().before(dataAtual)||dateChooserDataDevolucao.getDate().equals(dataAtual)) {
			DateFormat format = new SimpleDateFormat("dd/MM/yyy");
			JOptionPane.showMessageDialog(null, "Data de Devolu��o "+format.format(dateChooserDataDevolucao.getDate())+" deve ser maior que a data Atual "+format.format(dataAtual), null, JOptionPane.WARNING_MESSAGE);
			format = null;
			return false;
		}
		
		return true;
	}
	
	public void cleanFields(){
		fieldCodigo.setText(null);
		fieldCliente.setText(null);
		fieldData.setText(null);
		fieldStatus.setText(null);
		fieldMotorista.setText(null);
		fieldVeiculo.setText(null);
		comboBoxEmpresaDevolucao.setSelectedIndex(0);
		dateChooserDataDevolucao.setDate(dataAtual);
		chckbxCancelarLocacao.setSelected(false);
		fieldKms.setText(null);
	}
	
	public void enableFields(){
		comboBoxEmpresaDevolucao.setEnabled(true);
		buttonLocalizarMotorista.setEnabled(true);
		buttonLocalizaVeiculo.setEnabled(true);
		buttonLocalizaCliente.setEnabled(true);
		dateChooserDataDevolucao.setEnabled(true);
		comboBoxTipoLocacao.setEnabled(true);
		
	}
	
	public void disableFields(){
		comboBoxEmpresaDevolucao.setEnabled(false);
		buttonLocalizarMotorista.setEnabled(false);
		buttonLocalizaVeiculo.setEnabled(false);
		buttonLocalizaCliente.setEnabled(false);
		dateChooserDataDevolucao.setEnabled(false);
		chckbxCancelarLocacao.setEnabled(false);
		comboBoxTipoLocacao.setEnabled(false);
		fieldKms.setEditable(false);
	}
	
	public void setFields(int id){
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		locacaoUpdate = locacaoDAO.consultaUpdate(id);
		fieldCodigo.setText(String.valueOf(locacaoUpdate.getID()));
		fieldCliente.setText(locacaoUpdate.getCliente().getNome());
		try {
			fieldData.setText(String.valueOf(format.format(locacaoUpdate.getDataHoraSaida())));
		} catch (Exception e) {
		}
		
		fieldStatus.setText(locacaoUpdate.getStatus());
		fieldMotorista.setText(locacaoUpdate.getMotorista().getNome());
		fieldVeiculo.setText(locacaoUpdate.getVeiculo().getID()+" - Placa: "+locacaoUpdate.getVeiculo().getPlaca()+" - Chassi: "+locacaoUpdate.getVeiculo().getNumeroChassi());
		fieldValor.setText(String.valueOf(locacaoUpdate.getValor()));
		comboBoxTipoLocacao.setSelectedItem(locacaoUpdate.getTipo());
		dateChooserDataDevolucao.setDate(locacaoUpdate.getDataPrevistaDevolucao());
		cliente = locacaoUpdate.getCliente();
		motorista = locacaoUpdate.getMotorista();
		veiculo = locacaoUpdate.getVeiculo();
		comboBoxEmpresaDevolucao.setSelectedItem(locacaoUpdate.getEmpresaDestino().getId()+" - "+locacaoUpdate.getEmpresaDestino().getNome());
		dateChooserDataDevolucao.setDate(locacaoUpdate.getDataPrevistaDevolucao());
		buttonExcluir.setEnabled(true);
		buttonSalvar.setEnabled(true);
		buttonNovo.setEnabled(false);
		saveupdate = false;
		enableFields();
		chckbxCancelarLocacao.setEnabled(true);
		format = null;
	}
	
	public void setCliente(Cliente cliente){
		this.cliente = cliente;
		fieldCliente.setText(cliente.getId()+" - "+cliente.getNome());
	}
	
	public void setMotorista(PessoaFisica motorista){
		this.motorista = motorista;
		fieldMotorista.setText(motorista.getId()+" - "+motorista.getNome());
	}
	
	public void setVeiculo(Veiculo veiculo){
		if(reservaImportada!=null){
			if (veiculo.getCategoria().getID()==(reservaImportada.getCategoria().getID())){
				this.veiculo = veiculo;
				fieldVeiculo.setText(veiculo.getID()+" - Placa: "+veiculo.getPlaca()+" - Chassi: "+veiculo.getNumeroChassi());
				fieldValor.setText(String.valueOf(veiculo.getCategoria().getValorAluguel()));
			}
			else{
				JOptionPane.showMessageDialog(null, "Ve�culo Selecionado n�o pertence a Categoria Reservada");
			}
		} else{
			this.veiculo = veiculo;
			fieldVeiculo.setText(veiculo.getID()+" - Placa: "+veiculo.getPlaca()+" - Chassi: "+veiculo.getNumeroChassi());
			fieldValor.setText(String.valueOf(veiculo.getCategoria().getValorAluguel()));
		}
	}
	
	public void setReserva(Reserva reserva){
		this.reservaImportada = reserva;
		fieldCliente.setText(reserva.getCliente().getNome());
		cliente = reserva.getCliente();
	}
	
	public void atualizaValor(){
		if(fieldValor.getText().isEmpty()&&valorTotal==0){
			JOptionPane.showMessageDialog(null, "N�o existe valores a serem atualizados","Aviso Valores", JOptionPane.WARNING_MESSAGE);
		}else{
				if(comboBoxTipoLocacao.getSelectedIndex()==0){
					valorTotal = veiculo.getCategoria().getValorAluguel()+tiposLocacao.get(comboBoxTipoLocacao.getSelectedIndex()).getValor();
					fieldValor.setText(String.valueOf(valorTotal));
				}
				else{
					System.out.println(valorTotal);
					valorTotal = veiculo.getCategoria().getValorAluguel()+(tiposLocacao.get(comboBoxTipoLocacao.getSelectedIndex()).getValor()*Integer.parseInt(fieldKms.getText()));
					fieldValor.setText(String.valueOf(valorTotal));
				}
			}
	}
	
	public void setPosicao() {
		Dimension d = this.getDesktopPane().getSize();
		this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2); 
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JPanel getPanelCentro() {
		return panelCentro;
	}

	public JLabel getLabelCodigo() {
		return labelCodigo;
	}

	public JLabel getLabelData() {
		return labelData;
	}

	public JLabel getLabelStatus() {
		return labelStatus;
	}

	public JLabel getLabelEmpresaDevolucao() {
		return labelEmpresaDevolucao;
	}

	public JLabel getLabelMotorista() {
		return labelMotorista;
	}

	public JLabel getLabelCliente() {
		return labelCliente;
	}

	public JLabel getLabelDataDeRetirada() {
		return labelDataDeRetirada;
	}

	public JLabel getLabelValor() {
		return labelValor;
	}

	public JLabel getLblTipo() {
		return lblTipo;
	}

	public JLabel getLabelRS() {
		return labelRS;
	}

	public JLabel getLabelKms() {
		return labelKms;
	}

	public JLabel getLblVeculo() {
		return lblVeculo;
	}

	public JTextField getFieldCodigo() {
		return fieldCodigo;
	}

	public JTextField getFieldCliente() {
		return fieldCliente;
	}

	public JTextField getFieldData() {
		return fieldData;
	}

	public JTextField getFieldStatus() {
		return fieldStatus;
	}

	public JTextField getFieldMotorista() {
		return fieldMotorista;
	}

	public JTextField getFieldValor() {
		return fieldValor;
	}

	public JTextField getFieldKms() {
		return fieldKms;
	}

	public JTextField getFieldVeiculo() {
		return fieldVeiculo;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getComboBoxEmpresaDevolucao() {
		return comboBoxEmpresaDevolucao;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getComboBoxTipoLocacao() {
		return comboBoxTipoLocacao;
	}

	public JCheckBox getChckbxCancelarLocacao() {
		return chckbxCancelarLocacao;
	}

	public JButton getButtonSalvar() {
		return buttonSalvar;
	}

	public JButton getButtonCancelar() {
		return buttonCancelar;
	}

	public JButton getButtonExcluir() {
		return buttonExcluir;
	}

	public JButton getButtonLocalizar() {
		return buttonLocalizar;
	}

	public JButton getButtonNovo() {
		return buttonNovo;
	}

	public JButton getButtonLocalizarMotorista() {
		return buttonLocalizarMotorista;
	}

	public JButton getButtonLocalizaVeiculo() {
		return buttonLocalizaVeiculo;
	}

	public JButton getButtonLocalizaCliente() {
		return buttonLocalizaCliente;
	}

	public JDateChooser getDateChooserDataDevolucao() {
		return dateChooserDataDevolucao;
	}

	public boolean isSaveupdate() {
		return saveupdate;
	}

	public void setSaveupdate(boolean saveupdate) {
		this.saveupdate = saveupdate;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public Date getDataAtual() {
		return dataAtual;
	}

	public LocacaoDAO getLocacaoDAO() {
		return locacaoDAO;
	}

	public EmpresaDAO getEmpresaDAO() {
		return empresaDAO;
	}

	public Locacao getLocacaoUpdate() {
		return locacaoUpdate;
	}

	public void setLocacaoUpdate(Locacao locacaoUpdate) {
		this.locacaoUpdate = locacaoUpdate;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public List<TiposLocacao> getTiposLocacao() {
		return tiposLocacao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public PessoaFisica getMotorista() {
		return motorista;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public Reserva getReservaImportada() {
		return reservaImportada;
	}

	public ButtonHandlerTelaLocacao getButtonHandlerTelaLocacao() {
		return buttonHandlerTelaLocacao;
	}
}
