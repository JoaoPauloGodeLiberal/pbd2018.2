package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.Locadora.model.Locacao;
import br.com.Locadora.view.TelaConsultaLocacao;
import br.com.Locadora.view.TelaLocacao;

public class ButtonHandlerConsultaLocacao implements ActionListener {
	
	private TelaConsultaLocacao consultaLocacao;
	private TelaLocacao telaLocacao;
	
	public ButtonHandlerConsultaLocacao(TelaConsultaLocacao consultaLocacao, TelaLocacao telaLocacao) {
		this.consultaLocacao=consultaLocacao;
		this.telaLocacao=telaLocacao;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==consultaLocacao.getButtonResearch()) {
			if (consultaLocacao.getFieldCliente().getText().isEmpty()&&consultaLocacao.getFieldID().getText().isEmpty()) {
				consultaLocacao.getModelTalble().setNumRows(0);
				List<Locacao> locacoes = consultaLocacao.getLocacaoDAO().listALL();
				if (!locacoes.isEmpty()) {
					for (int i = 0; i < locacoes.size(); i++) {
						consultaLocacao.getModelTalble().addRow(new Object[]{locacoes.get(i).getID(), locacoes.get(i).getCliente().getNome(), locacoes.get(i).getDataHoraSaida(), locacoes.get(i).getDataPrevistaDevolucao()});
					}
					
					consultaLocacao.getButtonSelect().setEnabled(true);
				}
				else
					JOptionPane.showMessageDialog(consultaLocacao, "Nenhuma Loca��o Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				locacoes = null;
				
			}else if (!consultaLocacao.getFieldCliente().getText().isEmpty()) {
				consultaLocacao.getModelTalble().setNumRows(0);
				List<Locacao> locacoes = consultaLocacao.getLocacaoDAO().consultaByCliente(consultaLocacao.getFieldCliente().getText());
				if (!locacoes.isEmpty()) {
					for (int i = 0; i < locacoes.size(); i++) {
						consultaLocacao.getModelTalble().addRow(new Object[]{locacoes.get(i).getID(), locacoes.get(i).getCliente().getNome(), locacoes.get(i).getDataHoraSaida(), locacoes.get(i).getDataPrevistaDevolucao()});
					}
					
					consultaLocacao.getButtonSelect().setEnabled(true);
				}
				else
					JOptionPane.showMessageDialog(consultaLocacao, "Nenhuma Loca��o Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				locacoes = null;
			}else {
				consultaLocacao.getModelTalble().setNumRows(0);
				Locacao locacao = consultaLocacao.getLocacaoDAO().consultaId(Integer.parseInt(consultaLocacao.getFieldID().getText()));
				if (locacao != null) {
					consultaLocacao.getModelTalble().addRow(new Object[]{locacao.getID(), locacao.getCliente().getNome(), locacao.getDataHoraSaida(), locacao.getDataPrevistaDevolucao()});
					consultaLocacao.getButtonSelect().setEnabled(true);
				}
				else
					JOptionPane.showMessageDialog(consultaLocacao, "Nenhuma Loca��o Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				locacao = null;
			}
		}if(e.getSource()==consultaLocacao.getButtonSelect()) {
			telaLocacao.setFields((int) (consultaLocacao.getTableLocacao().getValueAt(consultaLocacao.getTableLocacao().getSelectedRow(), 0)));	
			consultaLocacao.dispose();
		}
		
	}

}
