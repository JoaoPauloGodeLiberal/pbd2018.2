package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.com.Locadora.model.Locacao;
import br.com.Locadora.view.TelaConsultaCliente;
import br.com.Locadora.view.TelaConsultaLocacao;
import br.com.Locadora.view.TelaConsultaVeiculo;
import br.com.Locadora.view.TelaLocacao;

public class ButtonHandlerTelaLocacao implements ActionListener {
	
	private TelaLocacao telaLocacao;
	
	public ButtonHandlerTelaLocacao(TelaLocacao telaLocacao) {
		this.telaLocacao = telaLocacao;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==telaLocacao.getButtonNovo()) {
			telaLocacao.cleanFields();
			telaLocacao.getButtonSalvar().setEnabled(true);
			telaLocacao.getButtonExcluir().setEnabled(false);
			telaLocacao.getButtonLocalizar().setEnabled(false);
			telaLocacao.getButtonNovo().setEnabled(false);
			telaLocacao.enableFields();
			telaLocacao.setSaveupdate(true);
		}
		
		if(e.getSource()==telaLocacao.getButtonSalvar()) {
			if (telaLocacao.validarFields()) {
				telaLocacao.atualizaValor();
				if(telaLocacao.isSaveupdate()){ 
					Locacao locacao = new Locacao();
					locacao.setCliente(telaLocacao.getCliente());
					locacao.setMotorista(telaLocacao.getMotorista());
					locacao.setVeiculo(telaLocacao.getVeiculo());
					locacao.setDataPrevistaDevolucao(telaLocacao.getDateChooserDataDevolucao().getDate());
					locacao.setTipo((String) telaLocacao.getComboBoxTipoLocacao().getSelectedItem());
					locacao.setValor(Double.parseDouble(telaLocacao.getFieldValor().getText()));
					locacao.setEmpresaDestino(telaLocacao.getEmpresas().get(telaLocacao.getComboBoxEmpresaDevolucao().getSelectedIndex()));
					if(telaLocacao.getReservaImportada() != null)
						locacao.setIdReservaImportada(telaLocacao.getReservaImportada().getID());
					if(telaLocacao.getLocacaoDAO().insert(locacao)){
						JOptionPane.showMessageDialog(null, "Loca��o Realizada com Sucesso", "Mensagem Loca��o", JOptionPane.INFORMATION_MESSAGE);
						telaLocacao.cleanFields();
						telaLocacao.disableFields();
						telaLocacao.getButtonNovo().setEnabled(true);
						telaLocacao.getButtonExcluir().setEnabled(false);
						telaLocacao.getButtonSalvar().setEnabled(false);
						telaLocacao.getButtonLocalizar().setEnabled(true);
					}
					else
						JOptionPane.showMessageDialog(null, "Erro ao Realizar Loca��o", "Erro Inser��o", JOptionPane.ERROR_MESSAGE);
				} else{ 
					telaLocacao.getLocacaoUpdate().setID(Integer.parseInt(telaLocacao.getFieldCodigo().getText()));
					telaLocacao.getLocacaoUpdate().setCliente(telaLocacao.getCliente());
					telaLocacao.getLocacaoUpdate().setVeiculo(telaLocacao.getVeiculo());
					telaLocacao.getLocacaoUpdate().setMotorista(telaLocacao.getMotorista());
					telaLocacao.getLocacaoUpdate().setEmpresaDestino(telaLocacao.getEmpresas().get(telaLocacao.getComboBoxEmpresaDevolucao().getSelectedIndex()));
					telaLocacao.getLocacaoUpdate().setDataPrevistaDevolucao(telaLocacao.getDateChooserDataDevolucao().getDate());
					telaLocacao.getLocacaoUpdate().setCancelada(telaLocacao.getChckbxCancelarLocacao().isSelected());
					if(telaLocacao.getLocacaoDAO().commit()){
						JOptionPane.showMessageDialog(null, "Loca��o Alterada com Sucesso", "Mensagem Loca��o", JOptionPane.INFORMATION_MESSAGE);
						telaLocacao.setLocacaoUpdate(null);
						telaLocacao.cleanFields();
						telaLocacao.disableFields();
						telaLocacao.getButtonNovo().setEnabled(true);
						telaLocacao.getButtonExcluir().setEnabled(false);
						telaLocacao.getButtonSalvar().setEnabled(false);
						telaLocacao.getButtonLocalizar().setEnabled(true);
					}
					else
						JOptionPane.showMessageDialog(null, "Erro ao Alterar Loca��o", "Erro Autaliza��o", JOptionPane.ERROR_MESSAGE);
					
				}
			}
		}
		
		if(e.getSource()==telaLocacao.getButtonExcluir()) {
			if (JOptionPane.showConfirmDialog(null, "Deseja Realmente Excluir ?", "Excluir Loca��o", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==0) {
				if(telaLocacao.getLocacaoDAO().delete(Integer.parseInt(telaLocacao.getFieldCodigo().getText()))){
					JOptionPane.showMessageDialog(null, "Loca��o Exclu�da com Sucesso", "Mensagem Loca��o", JOptionPane.INFORMATION_MESSAGE);
					telaLocacao.cleanFields();
					telaLocacao.disableFields();
					telaLocacao.getButtonNovo().setEnabled(true);
					telaLocacao.getButtonExcluir().setEnabled(false);
					telaLocacao.getButtonSalvar().setEnabled(false);
				}
				else
					JOptionPane.showMessageDialog(null, "Erro ao Deletar Loca��o", "Erro Remo��o", JOptionPane.ERROR_MESSAGE);

			}
		}
		
		if(e.getSource()==telaLocacao.getButtonCancelar()) {
			telaLocacao.cleanFields();
			telaLocacao.disableFields();
			telaLocacao.getButtonSalvar().setEnabled(false);
			telaLocacao.getButtonNovo().setEnabled(true);
			telaLocacao.getButtonExcluir().setEnabled(false);
			telaLocacao.getButtonLocalizar().setEnabled(true);
		}
		
		if(e.getSource()==telaLocacao.getButtonLocalizar()) {
			new TelaConsultaLocacao(telaLocacao).setVisible(true);
		}
		
		if(e.getSource()==telaLocacao.getButtonLocalizarMotorista()) {
			new TelaConsultaCliente(telaLocacao, false).setVisible(true);
		}
		
		if(e.getSource()==telaLocacao.getButtonLocalizaCliente()) {
			new TelaConsultaCliente(telaLocacao, true).setVisible(true);
		}
		
		if(e.getSource()==telaLocacao.getButtonLocalizaVeiculo()) {
			new TelaConsultaVeiculo(telaLocacao).setVisible(true);
		}
		
		if(e.getSource()==telaLocacao.getComboBoxTipoLocacao()) {
			if (telaLocacao.getComboBoxTipoLocacao().getSelectedItem().equals("Km Controle")) 
				telaLocacao.getFieldKms().setEditable(true);
			else 
				telaLocacao.getFieldKms().setEditable(false);
			telaLocacao.getFieldKms().setText(null);
		}
		
	}

}
