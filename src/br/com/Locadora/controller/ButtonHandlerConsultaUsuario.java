package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.Locadora.dao.UsuarioDAO;
import br.com.Locadora.model.Usuario;
import br.com.Locadora.view.TelaCadastroUsuarioOK;
import br.com.Locadora.view.TelaConsultaUsuario;

public class ButtonHandlerConsultaUsuario implements ActionListener {
	
	private TelaConsultaUsuario consultaUsuario;
	private UsuarioDAO usuarioDAO;
	private TelaCadastroUsuarioOK telaCadastroUsuarioOK;
	
	public ButtonHandlerConsultaUsuario(TelaConsultaUsuario consultaUsuario, TelaCadastroUsuarioOK telaCadastroUsuarioOK) {
		this.consultaUsuario = consultaUsuario;
		this.telaCadastroUsuarioOK = telaCadastroUsuarioOK;
		this.usuarioDAO = new UsuarioDAO();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==consultaUsuario.getButtonResearch()) {
			usuarioDAO = new UsuarioDAO();
			
			if (consultaUsuario.getFieldNome().getText().isEmpty()&&consultaUsuario.getFieldID().getText().isEmpty()) {
				consultaUsuario.getModelTalble().setNumRows(0);
				List<Usuario> usuarios = usuarioDAO.listarTodos();
				if (!usuarios.isEmpty()) {
					for (int i = 0; i < usuarios.size(); i++) {
						consultaUsuario.getModelTalble().addRow(new Object[]{usuarios.get(i).getId(), usuarios.get(i).getLogin(),usuarios.get(i).getNome()});
					}
					consultaUsuario.getButtonSelect().setEnabled(true);
				}else
					JOptionPane.showMessageDialog(null, "Nenhum Usu�rio Encontrado", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				usuarios = null;	
				
			}else if (!consultaUsuario.getFieldNome().getText().isEmpty()) {
				consultaUsuario.getModelTalble().setNumRows(0);
				List<Usuario> usuarios = usuarioDAO.consultaNome(consultaUsuario.getFieldNome().getText());
				if (!usuarios.isEmpty()) {
					for (int i = 0; i < usuarios.size(); i++) {
						consultaUsuario.getModelTalble().addRow(new Object[]{usuarios.get(i).getId(), usuarios.get(i).getLogin(),usuarios.get(i).getNome()});
					}
					consultaUsuario.getButtonSelect().setEnabled(true);
				}else
					JOptionPane.showMessageDialog(null, "Nenhum Usu�rio Encontrado", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				usuarios = null;
			}else {
				consultaUsuario.getModelTalble().setNumRows(0);
				Usuario usuario = usuarioDAO.consultaId(Integer.parseInt(consultaUsuario.getFieldID().getText()));
				if (usuario != null) {
					consultaUsuario.getModelTalble().addRow(new Object[]{usuario.getId(),usuario.getNome()});
					consultaUsuario.getButtonSelect().setEnabled(true);
				}else
					JOptionPane.showMessageDialog(null, "Nenhum Usu�rio Encontrado", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				usuario = null;
			}
		}if(e.getSource()==consultaUsuario.getButtonSelect()) {
			telaCadastroUsuarioOK.setFields((int) (consultaUsuario.getTableUsuarios().getValueAt(consultaUsuario.getTableUsuarios().getSelectedRow(), 0)));
			consultaUsuario.dispose();
		}
		
	}

}
