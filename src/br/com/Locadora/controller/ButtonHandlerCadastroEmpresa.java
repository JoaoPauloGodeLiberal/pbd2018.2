package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.com.Locadora.model.Empresa;
import br.com.Locadora.view.TelaCadastroEmpresaOK;
import br.com.Locadora.view.TelaConsultaEmpresa;

public class ButtonHandlerCadastroEmpresa implements ActionListener {
	
	TelaCadastroEmpresaOK telaCadastroEmpresa;
	
	public ButtonHandlerCadastroEmpresa(TelaCadastroEmpresaOK telaCadastroEmpresa) {
		this.telaCadastroEmpresa=telaCadastroEmpresa;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==telaCadastroEmpresa.getButtonNovo()) {
			telaCadastroEmpresa.cleanFields();
			telaCadastroEmpresa.getButtonSalvar().setEnabled(true);
			telaCadastroEmpresa.getButtonExcluir().setEnabled(false);
			telaCadastroEmpresa.getButtonLocalizar().setEnabled(false);
			telaCadastroEmpresa.getButtonNovo().setEnabled(false);
			telaCadastroEmpresa.enableFields();
			telaCadastroEmpresa.setSaveupdate(true);
		}
		
		if(e.getSource()==telaCadastroEmpresa.getButtonSalvar()) {
			if (telaCadastroEmpresa.validarFields()) {
				Empresa emp = new Empresa();
				emp.setNome(telaCadastroEmpresa.getFieldNome().getText());
				emp.setEndBairro(telaCadastroEmpresa.getFieldBairro().getText());
				emp.setEndRua(telaCadastroEmpresa.getFieldRua().getText());
				emp.setFilial(telaCadastroEmpresa.getChckbxFilial().isSelected());
				emp.setEndCidade(telaCadastroEmpresa.getFieldCidade().getText());
				emp.setEndNumero(telaCadastroEmpresa.getFieldNumero().getText());
				emp.setEndEstado((String) telaCadastroEmpresa.getComboBoxEstado().getSelectedItem());
				
				if(telaCadastroEmpresa.isSaveupdate()){ 
					if(telaCadastroEmpresa.getEmpresaDAO().insert(emp)){
						JOptionPane.showMessageDialog(null, "Empresa Cadastrada com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
						telaCadastroEmpresa.cleanFields();
						telaCadastroEmpresa.disableFields();
						telaCadastroEmpresa.getButtonNovo().setEnabled(true);
						telaCadastroEmpresa.getButtonExcluir().setEnabled(false);
						telaCadastroEmpresa.getButtonSalvar().setEnabled(false);
						telaCadastroEmpresa.getButtonLocalizar().setEnabled(true);
					}else
						JOptionPane.showMessageDialog(null, "Erro ao Cadastrar Empresa", "Erro Inser��o", JOptionPane.ERROR_MESSAGE);
				} else{ 
					emp.setId(Integer.parseInt(telaCadastroEmpresa.getFieldCodigo().getText()));
					if(telaCadastroEmpresa.getEmpresaDAO().update(emp)){
						JOptionPane.showMessageDialog(null, "Empresa Alterada com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
						telaCadastroEmpresa.cleanFields();
						telaCadastroEmpresa.disableFields();
						telaCadastroEmpresa.getButtonNovo().setEnabled(true);
						telaCadastroEmpresa.getButtonExcluir().setEnabled(false);
						telaCadastroEmpresa.getButtonSalvar().setEnabled(false);
						telaCadastroEmpresa.getButtonLocalizar().setEnabled(true);
					}else
						JOptionPane.showMessageDialog(null, "Erro ao Alterar Empresa", "Erro Autaliza��o", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		
		if(e.getSource()==telaCadastroEmpresa.getButtonExcluir()) {
			if (JOptionPane.showConfirmDialog(null, "Deseja Realmente Excluir ?", "Excluir Empresa", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==0) {
				if(telaCadastroEmpresa.getEmpresaDAO().delete(Integer.parseInt(telaCadastroEmpresa.getFieldCodigo().getText()))){
					JOptionPane.showMessageDialog(null, "Empresa Exclu�da com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
					telaCadastroEmpresa.cleanFields();
					telaCadastroEmpresa.disableFields();
					telaCadastroEmpresa.getButtonNovo().setEnabled(true);
					telaCadastroEmpresa.getButtonExcluir().setEnabled(false);
					telaCadastroEmpresa.getButtonSalvar().setEnabled(false);
				}else
					JOptionPane.showMessageDialog(null, "Erro ao Deletar Empresa", "Erro Remo��o", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(e.getSource()==telaCadastroEmpresa.getButtonCancelar()) {
			telaCadastroEmpresa.cleanFields();
			telaCadastroEmpresa.disableFields();
			telaCadastroEmpresa.getButtonSalvar().setEnabled(false);
			telaCadastroEmpresa.getButtonNovo().setEnabled(true);
			telaCadastroEmpresa.getButtonExcluir().setEnabled(false);
			telaCadastroEmpresa.getButtonLocalizar().setEnabled(true);
		}
		
		if(e.getSource()==telaCadastroEmpresa.getButtonLocalizar()) {
			new TelaConsultaEmpresa(telaCadastroEmpresa).setVisible(true);
		}
		
	}

}
