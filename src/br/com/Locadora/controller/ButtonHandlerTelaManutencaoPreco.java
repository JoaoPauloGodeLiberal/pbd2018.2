package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.com.Locadora.view.TelaManutencaoPreco;

public class ButtonHandlerTelaManutencaoPreco implements ActionListener {
	
	TelaManutencaoPreco telaManutencaoPreco;

	public ButtonHandlerTelaManutencaoPreco(TelaManutencaoPreco telaManutencaoPreco) {
		this.telaManutencaoPreco = telaManutencaoPreco;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==telaManutencaoPreco.getButtonSalvar()) {
			if (telaManutencaoPreco.validarFields()) {
				telaManutencaoPreco.getTiposLocacaos().get(0).setValor(Double.parseDouble(telaManutencaoPreco.getFieldPrecoKml().getText().replace(',', '.')));
				telaManutencaoPreco.getTiposLocacaos().get(1).setValor(Double.parseDouble(telaManutencaoPreco.getTextFieldPrecoKmc().getText().replace(',', '.')));
				if(telaManutencaoPreco.getTiposLocacaoDAO().update(telaManutencaoPreco.getTiposLocacaos()))
					JOptionPane.showMessageDialog(null, "Pre�os Alterados com Sucesso", "Mensagem Pre�os", JOptionPane.INFORMATION_MESSAGE);
				else
					JOptionPane.showMessageDialog(null, "Erro ao Alterar Pre�os", "Erro Altera��o", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(e.getSource()==telaManutencaoPreco.getButtonCancelar()) {
			telaManutencaoPreco.getTextFieldPrecoKmc().setText(String.valueOf(telaManutencaoPreco.getTiposLocacaos().get(1).getValor()));
			telaManutencaoPreco.getFieldPrecoKml().setText(String.valueOf(telaManutencaoPreco.getTiposLocacaos().get(0).getValor()));
		}		
	}
}
