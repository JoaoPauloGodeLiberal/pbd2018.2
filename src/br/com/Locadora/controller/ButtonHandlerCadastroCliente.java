package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.com.Locadora.model.PessoaFisica;
import br.com.Locadora.model.PessoaJuridica;
import br.com.Locadora.view.TelaCadastroClienteOK;
import br.com.Locadora.view.TelaConsultaCliente;

public class ButtonHandlerCadastroCliente implements ActionListener {

	private TelaCadastroClienteOK telaCadastroCliente;

	public ButtonHandlerCadastroCliente(TelaCadastroClienteOK telaCadastroCliente) {
		this.telaCadastroCliente=telaCadastroCliente;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==telaCadastroCliente.getButtonNovo()) {
			telaCadastroCliente.cleanFields();
			telaCadastroCliente.getButtonSalvar().setEnabled(true);
			telaCadastroCliente.getButtonExcluir().setEnabled(false);
			telaCadastroCliente.getButtonLocalizar().setEnabled(false);
			telaCadastroCliente.getButtonNovo().setEnabled(false);
			telaCadastroCliente.getRadiobuttonPFisica().setEnabled(true);
			telaCadastroCliente.getRadiobuttonPJurdica().setEnabled(true);
			if (telaCadastroCliente.getRadiobuttonPFisica().isSelected()) {
				telaCadastroCliente.enableFieldsPF();
			}else {
				telaCadastroCliente.enableFieldsPJ();
			}
			telaCadastroCliente.enableFieldsEnd();
			telaCadastroCliente.setSaveupdate(true);
		}
		
		//---------------------
		
		if(e.getSource()==telaCadastroCliente.getButtonSalvar()) {
			if (telaCadastroCliente.validaFields()) {
				if(telaCadastroCliente.getRadiobuttonPFisica().isSelected()){
					String s = (String) telaCadastroCliente.getComboBoxSexo().getSelectedItem();
					char sexo = s.charAt(0);
					PessoaFisica pf = new PessoaFisica();
					try {
						pf.setNome(telaCadastroCliente.getFieldNome().getText());
						pf.setCpf(telaCadastroCliente.getFieldCPF().getText().replaceAll("[.-]", ""));
						pf.setNumeroHabilitacao(telaCadastroCliente.getFieldHabilitacao().getText());
						pf.setBairro(telaCadastroCliente.getFieldBairro().getText());
						pf.setRua(telaCadastroCliente.getFieldRua().getText());
						pf.setnumeroEndereco(telaCadastroCliente.getFieldNumero().getText());
						pf.setCidade(telaCadastroCliente.getFieldCidade().getText());
						pf.setEstado((String) telaCadastroCliente.getComboBox().getSelectedItem());
						pf.setSexo(sexo);
						pf.setDataNascimento(telaCadastroCliente.getDateChooserNascimento().getDate());
						pf.setDataVencimentoHailitacao(telaCadastroCliente.getDateChooserCNH().getDate());

						if(telaCadastroCliente.isSaveupdate()){ 
							if(telaCadastroCliente.getClienteDAO().insert(pf)){
								JOptionPane.showMessageDialog(null, "Cliente Cadastrado com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
								telaCadastroCliente.cleanFields();
								telaCadastroCliente.disableFields();
								telaCadastroCliente.getButtonNovo().setEnabled(true);
								telaCadastroCliente.getButtonSalvar().setEnabled(false);
								telaCadastroCliente.getButtonLocalizar().setEnabled(true);
								telaCadastroCliente.getButtonExcluir().setEnabled(false);
								telaCadastroCliente.getRadiobuttonPFisica().setEnabled(false);
								telaCadastroCliente.getRadiobuttonPJurdica().setEnabled(false);
							}else {
								JOptionPane.showMessageDialog(null, "Erro ao Cadastrar Cliente", "Erro Inser��o", JOptionPane.ERROR_MESSAGE);
							}

						} else{ 
							pf.setId(Integer.parseInt(telaCadastroCliente.getFieldCodigo().getText()));
							if(telaCadastroCliente.getClienteDAO().updatePF(pf)){
								JOptionPane.showMessageDialog(null, "Cliente Alterado com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
								telaCadastroCliente.cleanFields();
								telaCadastroCliente.disableFields();
								telaCadastroCliente.getButtonNovo().setEnabled(true);
								telaCadastroCliente.getButtonSalvar().setEnabled(false);
								telaCadastroCliente.getButtonLocalizar().setEnabled(true);
								telaCadastroCliente.getButtonExcluir().setEnabled(false);
								telaCadastroCliente.getRadiobuttonPFisica().setEnabled(false);
								telaCadastroCliente.getRadiobuttonPJurdica().setEnabled(false);
							}else {
								JOptionPane.showMessageDialog(null, "Erro ao Alterar Cliente", "Erro Autaliza��o", JOptionPane.ERROR_MESSAGE);
							}
						}

					} catch (Exception exception) {
						JOptionPane.showMessageDialog(null, "CPF Inv�lido","Aviso de Inconsist�ncia" , JOptionPane.WARNING_MESSAGE);
					}

				}else {
					PessoaJuridica pj = new PessoaJuridica();
					try {
						pj.setNome(telaCadastroCliente.getFieldNome().getText());
						pj.setCnpj(telaCadastroCliente.getFieldCNPJ().getText().replaceAll("\\D", ""));
						pj.setInscricaoEstadual(telaCadastroCliente.getFieldInscEstadual().getText());
						pj.setBairro(telaCadastroCliente.getFieldBairro().getText());
						pj.setnumeroEndereco((telaCadastroCliente.getFieldNumero().getText()));
						pj.setRua(telaCadastroCliente.getFieldRua().getText());
						pj.setCidade(telaCadastroCliente.getFieldCidade().getText());
						pj.setEstado((String) telaCadastroCliente.getComboBox().getSelectedItem());

						if(telaCadastroCliente.isSaveupdate()){ 
							if(telaCadastroCliente.getClienteDAO().insert(pj)){
								JOptionPane.showMessageDialog(null, "Cliente Cadastrado com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
								telaCadastroCliente.cleanFields();
								telaCadastroCliente.disableFields();
								telaCadastroCliente.getButtonNovo().setEnabled(true);
								telaCadastroCliente.getButtonSalvar().setEnabled(false);
								telaCadastroCliente.getButtonLocalizar().setEnabled(true);
								telaCadastroCliente.getButtonExcluir().setEnabled(false);
								telaCadastroCliente.getRadiobuttonPFisica().setEnabled(false);
								telaCadastroCliente.getRadiobuttonPJurdica().setEnabled(false);
							}else {
								JOptionPane.showMessageDialog(null, "Erro ao Cadastrar Cliente", "Erro Inser��o", JOptionPane.ERROR_MESSAGE);
							}
						} else{ 
							pj.setId(Integer.parseInt(telaCadastroCliente.getFieldCodigo().getText()));
							if(telaCadastroCliente.getClienteDAO().updatePJ(pj)){
								JOptionPane.showMessageDialog(null, "Cliente Alterado com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
								telaCadastroCliente.cleanFields();
								telaCadastroCliente.disableFields();
								telaCadastroCliente.getButtonNovo().setEnabled(true);
								telaCadastroCliente.getButtonSalvar().setEnabled(false);
								telaCadastroCliente.getButtonLocalizar().setEnabled(true);
								telaCadastroCliente.getButtonExcluir().setEnabled(false);
								telaCadastroCliente.getRadiobuttonPFisica().setEnabled(false);
								telaCadastroCliente.getRadiobuttonPJurdica().setEnabled(false);
							}else {
								JOptionPane.showMessageDialog(null, "Erro ao Alterar Cliente", "Erro Autaliza��o", JOptionPane.ERROR_MESSAGE);
							}
						}
					} catch (Exception exception) {
						JOptionPane.showMessageDialog(null, "CNPJ Inv�lido","Aviso de Inconsist�ncia" , JOptionPane.WARNING_MESSAGE);
					}

				}

			}
		}

		//---------------------
		
		if(e.getSource()==telaCadastroCliente.getButtonExcluir()) {
			if (JOptionPane.showConfirmDialog(null, "Deseja Realmente Excluir ?", "Excluir Usu�rio", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==0) {
				if(telaCadastroCliente.getRadiobuttonPFisica().isSelected()){
					if(telaCadastroCliente.getClienteDAO().deletePF(Integer.parseInt(telaCadastroCliente.getFieldCodigo().getText()))){
						//JOptionPane.showMessageDialog(null, "Cliente Exclu�do com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
						telaCadastroCliente.cleanFields();
						telaCadastroCliente.disableFields();
						telaCadastroCliente.getButtonNovo().setEnabled(true);
						telaCadastroCliente.getButtonExcluir().setEnabled(false);
						telaCadastroCliente.getButtonSalvar().setEnabled(false);
					} /*else{
						JOptionPane.showMessageDialog(null, "Erro ao Deletar Cliente", "Erro Remo��o", JOptionPane.ERROR_MESSAGE);
					}*/

				}else {
					if(telaCadastroCliente.getClienteDAO().deletePJ(Integer.parseInt(telaCadastroCliente.getFieldCodigo().getText()))){
						//JOptionPane.showMessageDialog(null, "Cliente Exclu�do com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
						telaCadastroCliente.cleanFields();
						telaCadastroCliente.disableFields();
						telaCadastroCliente.getButtonNovo().setEnabled(true);
						telaCadastroCliente.getButtonExcluir().setEnabled(false);
						telaCadastroCliente.getButtonSalvar().setEnabled(false);
					}/* else{
						JOptionPane.showMessageDialog(null, "Erro ao Deletar Cliente", "Erro Remo��o", JOptionPane.ERROR_MESSAGE);
					}*/
				}				

			}
		}

		//---------------------
		
		if(e.getSource()==telaCadastroCliente.getButtonCancelar()) {
			telaCadastroCliente.cleanFields();
			telaCadastroCliente.disableFields();
			telaCadastroCliente.getRadiobuttonPFisica().setSelected(true);
			telaCadastroCliente.getRadiobuttonPFisica().setEnabled(false);
			telaCadastroCliente.getRadiobuttonPJurdica().setEnabled(false);
			telaCadastroCliente.getButtonSalvar().setEnabled(false);
			telaCadastroCliente.getButtonNovo().setEnabled(true);
			telaCadastroCliente.getButtonExcluir().setEnabled(false);
			telaCadastroCliente.getButtonLocalizar().setEnabled(true);
		}
		
		//--------------------
		
		if(e.getSource()==telaCadastroCliente.getRadiobuttonPFisica()) {
			if (telaCadastroCliente.getRadiobuttonPFisica().isSelected()){
				telaCadastroCliente.enableFieldsPF();
			}
		}
		
		//----------------------
		
		
		if(e.getSource()==telaCadastroCliente.getRadiobuttonPJurdica()) {
			if (telaCadastroCliente.getRadiobuttonPJurdica().isSelected()){
				telaCadastroCliente.enableFieldsPJ();
			}
		}
		
		//----------------------
		
		if(e.getSource()==telaCadastroCliente.getButtonLocalizar()) {
			new TelaConsultaCliente(telaCadastroCliente, false).setVisible(true);
		}
		
	}
		
					
}


