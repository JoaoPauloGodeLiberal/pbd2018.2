package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.Locadora.model.Veiculo;
import br.com.Locadora.view.TelaCadastroVeiculoOK;
import br.com.Locadora.view.TelaConsultaVeiculo;
import br.com.Locadora.view.TelaLocacao;

public class ButtonHandlerConsultaVeiculos implements  ActionListener {
	
	private TelaConsultaVeiculo consultaVeiculo;
	private Object objectTela;
	private TelaCadastroVeiculoOK telaCadastroVeiculoOK;
	private TelaLocacao telaLocacao;

	public ButtonHandlerConsultaVeiculos(TelaConsultaVeiculo consultaVeiculo, Object objectTela, TelaCadastroVeiculoOK telaCadastroVeiculoOK, TelaLocacao telaLocacao) {
		this.consultaVeiculo = consultaVeiculo;
		this.objectTela = objectTela;
		this.telaCadastroVeiculoOK = telaCadastroVeiculoOK;
		this.telaLocacao = telaLocacao;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==consultaVeiculo.getButtonResearch()) {
			String tipo;
			if (consultaVeiculo.getFormattedTextFieldPlaca().getText().equals("   -    ")&&consultaVeiculo.getFieldCodigo().getText().isEmpty()) {
				consultaVeiculo.getModelTalble().setNumRows(0);
				List<Veiculo> veiculos = consultaVeiculo.getVeiculoDAO().ListAll();
				if (!veiculos.isEmpty()) {
					for (int i = 0; i < veiculos.size(); i++) {
						if(veiculos.get(i).getTipo()=='A')
							tipo = "Autom�vel";
						else if(veiculos.get(i).getTipo()=='C')
							tipo = "Caminhotene";
						else
							tipo = "Caminhotene de Carga";
						
						consultaVeiculo.getModelTalble().addRow(new Object[]{veiculos.get(i).getID(), veiculos.get(i).getNumeroChassi(),veiculos.get(i).getPlaca(), tipo, veiculos.get(i).getCategoria().getDescricao(), veiculos.get(i).getAnoModelo(), veiculos.get(i).getAnoFabricacao()});
					}
					consultaVeiculo.getButtonSelect().setEnabled(true);
				}else
					JOptionPane.showMessageDialog(consultaVeiculo, "Nenhum Ve�culo Encontrado","Aviso Busca",JOptionPane.WARNING_MESSAGE);
				
				veiculos = null;	
			}else if (!consultaVeiculo.getFormattedTextFieldPlaca().getText().equals("   -    ")) {
				consultaVeiculo.getModelTalble().setNumRows(0);
				Veiculo veiculo = consultaVeiculo.getVeiculoDAO().consultaPlaca(consultaVeiculo.getFormattedTextFieldPlaca().getText().replaceAll("[.-]", ""));
				if(veiculo!=null){
					if(veiculo.getTipo()=='A')
						tipo = "Autom�vel";
					else if(veiculo.getTipo()=='C')
						tipo = "Caminhotene";
					else
						tipo = "Caminhotene de Carga";
					consultaVeiculo.getModelTalble().addRow(new Object[]{veiculo.getID(), veiculo.getNumeroChassi(),veiculo.getPlaca(), tipo, veiculo.getCategoria().getDescricao(), veiculo.getAnoModelo(), veiculo.getAnoFabricacao()});
					consultaVeiculo.getButtonSelect().setEnabled(true);
				}else 
					JOptionPane.showMessageDialog(consultaVeiculo, "Nenhum Ve�culo Encontrado","Aviso Busca",JOptionPane.WARNING_MESSAGE);
				
				veiculo = null;
			}else {
				consultaVeiculo.getModelTalble().setNumRows(0);
				Veiculo veiculo = consultaVeiculo.getVeiculoDAO().consultaId(Integer.parseInt(consultaVeiculo.getFieldCodigo().getText()));
				if (veiculo != null) {
					if(veiculo.getTipo()=='A')
						tipo = "Autom�vel";
					else if(veiculo.getTipo()=='C')
						tipo = "Caminhotene";
					else
						tipo = "Caminhotene de Carga";
					consultaVeiculo.getModelTalble().addRow(new Object[]{veiculo.getID(), veiculo.getNumeroChassi(),veiculo.getPlaca(), tipo, veiculo.getCategoria().getDescricao(), veiculo.getAnoModelo(), veiculo.getAnoFabricacao()});
					consultaVeiculo.getButtonSelect().setEnabled(true);
				}else
					JOptionPane.showMessageDialog(consultaVeiculo, "Nenhum Ve�culo Encontrado","Aviso Busca",JOptionPane.WARNING_MESSAGE);
				
				veiculo = null;
			}
		}if(e.getSource()==consultaVeiculo.getButtonSelect()) {
			if (objectTela.getClass().equals(TelaCadastroVeiculoOK.class)) {
				consultaVeiculo.dispose();
				telaCadastroVeiculoOK = (TelaCadastroVeiculoOK) objectTela;
				telaCadastroVeiculoOK.setFields((Integer) (consultaVeiculo.getTableEmpresas().getValueAt(consultaVeiculo.getTableEmpresas().getSelectedRow(), 0)));
			}else {
				consultaVeiculo.dispose();
				telaLocacao = (TelaLocacao) objectTela;
				telaLocacao.setVeiculo(consultaVeiculo.getVeiculoDAO().consultaId((Integer) (consultaVeiculo.getTableEmpresas().getValueAt(consultaVeiculo.getTableEmpresas().getSelectedRow(), 0))));
			}
		}if(e.getSource()==consultaVeiculo.getButtonLimpar()) {
			consultaVeiculo.getModelTalble().setNumRows(0);
			consultaVeiculo.getFieldCodigo().setText(null);
			consultaVeiculo.getFormattedTextFieldPlaca().setText(null);
			consultaVeiculo.getButtonSelect().setEnabled(false);
		}
		
	}

}
