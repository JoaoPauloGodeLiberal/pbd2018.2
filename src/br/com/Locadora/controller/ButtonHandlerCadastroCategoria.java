package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.com.Locadora.dao.CategoriaDAO;
import br.com.Locadora.model.Categoria;
import br.com.Locadora.view.TelaCadastroCategoriaOK;
import br.com.Locadora.view.TelaConsultaCategoria;

public class ButtonHandlerCadastroCategoria implements ActionListener {
	
	TelaCadastroCategoriaOK telaCadastroCategoria;
	TelaConsultaCategoria telaConsultaCategoria;
	CategoriaDAO categoriaDAO;
	
	public ButtonHandlerCadastroCategoria(TelaCadastroCategoriaOK telaCadastroCategoria) {
		this.telaCadastroCategoria=telaCadastroCategoria;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==telaCadastroCategoria.getButtonNovo()) {
			telaCadastroCategoria.cleanFields();
			telaCadastroCategoria.getButtonSalvar().setEnabled(true);
			telaCadastroCategoria.getButtonExcluir().setEnabled(false);
			telaCadastroCategoria.getButtonLocalizar().setEnabled(false);
			telaCadastroCategoria.getButtonNovo().setEnabled(false);
			telaCadastroCategoria.enableFields();
			telaCadastroCategoria.setSaveupdate(true);
		}
		
		if(e.getSource()==telaCadastroCategoria.getButtonSalvar()) {
			if (telaCadastroCategoria.validarFields()) {
				Categoria categoria = new Categoria();
				categoria.setDescricao(telaCadastroCategoria.getFieldDescricao().getText());
				categoria.setValorAluguel(Double.parseDouble(telaCadastroCategoria.getFormattedTextFieldPreco().getText().replace(',', '.')));
				if(telaCadastroCategoria.isSaveupdate()){ 
					if(telaCadastroCategoria.getCategoriaDAO().insert(categoria)){
						JOptionPane.showMessageDialog(null, "Categoria Cadastrada com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
						telaCadastroCategoria.cleanFields();
						telaCadastroCategoria.disableFields();
						telaCadastroCategoria.getButtonNovo().setEnabled(true);
						telaCadastroCategoria.getButtonExcluir().setEnabled(false);
						telaCadastroCategoria.getButtonSalvar().setEnabled(false);
						telaCadastroCategoria.getButtonLocalizar().setEnabled(true);
						categoria = null;
					}
					else
						JOptionPane.showMessageDialog(null, "Erro ao Cadastrar Categoria", "Mensagem Cadastro", JOptionPane.ERROR_MESSAGE);
				} else{ 
					categoria.setID(Integer.parseInt(telaCadastroCategoria.getFieldCodigo().getText()));
					if(telaCadastroCategoria.getCategoriaDAO().update(categoria)){
						JOptionPane.showMessageDialog(null, "Categoria Alterada com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
						telaCadastroCategoria.cleanFields();
						telaCadastroCategoria.disableFields();
						telaCadastroCategoria.getButtonNovo().setEnabled(true);
						telaCadastroCategoria.getButtonExcluir().setEnabled(false);
						telaCadastroCategoria.getButtonSalvar().setEnabled(false);
						telaCadastroCategoria.getButtonLocalizar().setEnabled(true);
						categoria = null;
					}
					else
						JOptionPane.showMessageDialog(null, "Erro ao Alterar Categoria", "Mensagem Cadastro", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		}
		
		if(e.getSource()==telaCadastroCategoria.getButtonExcluir()) {
			if (JOptionPane.showConfirmDialog(null, "Deseja Realmente Excluir ?", "Excluir Categoria", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==0) {
				categoriaDAO = new CategoriaDAO();
				if(categoriaDAO.delete(Integer.parseInt(telaCadastroCategoria.getFieldCodigo().getText()))){
					JOptionPane.showMessageDialog(null, "Categoria Exclu�da com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
					telaCadastroCategoria.cleanFields();
					telaCadastroCategoria.disableFields();
					telaCadastroCategoria.getButtonNovo().setEnabled(true);
					telaCadastroCategoria.getButtonExcluir().setEnabled(false);
					telaCadastroCategoria.getButtonSalvar().setEnabled(false);
				}
				else
					JOptionPane.showMessageDialog(null, "Erro ao Deletar Categoria", "Erro Remo��o", JOptionPane.ERROR_MESSAGE);;
					
			}
		}
		
		if(e.getSource()==telaCadastroCategoria.getButtonCancelar()) {
			telaCadastroCategoria.cleanFields();
			telaCadastroCategoria.disableFields();
			telaCadastroCategoria.getButtonSalvar().setEnabled(false);
			telaCadastroCategoria.getButtonNovo().setEnabled(true);
			telaCadastroCategoria.getButtonExcluir().setEnabled(false);
			telaCadastroCategoria.getButtonLocalizar().setEnabled(true);
		}
		
		if(e.getSource()==telaCadastroCategoria.getButtonLocalizar()) {
			telaConsultaCategoria = new TelaConsultaCategoria(ButtonHandlerCadastroCategoria.this);
			telaConsultaCategoria.setVisible(true);
		}
		
		if(e.getSource()==telaConsultaCategoria.getButtonResearch()) {
			telaConsultaCategoria.pesquisar();
		}
		
		if(e.getSource()==telaConsultaCategoria.getButtonSelect()) {
			telaCadastroCategoria.setFields((int) (telaConsultaCategoria.getTableCategoria().getValueAt(telaConsultaCategoria.getTableCategoria().getSelectedRow(), 0)));
			telaConsultaCategoria.dispose();
		}
	}

}
