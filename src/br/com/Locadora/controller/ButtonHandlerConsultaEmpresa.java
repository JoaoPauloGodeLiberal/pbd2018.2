package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.Locadora.dao.EmpresaDAO;
import br.com.Locadora.model.Empresa;
import br.com.Locadora.view.TelaCadastroEmpresaOK;
import br.com.Locadora.view.TelaConsultaEmpresa;

public class ButtonHandlerConsultaEmpresa implements ActionListener {
	
	private TelaConsultaEmpresa consultaEmpresa;
	private TelaCadastroEmpresaOK telaCadastroEmpresaOK;
	
	public ButtonHandlerConsultaEmpresa(TelaConsultaEmpresa consultaEmpresa, TelaCadastroEmpresaOK telaCadastroEmpresaOK) {
		this.consultaEmpresa=consultaEmpresa;
		this.telaCadastroEmpresaOK=telaCadastroEmpresaOK;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==consultaEmpresa.getButtonResearch()) {
			EmpresaDAO empresaController = new EmpresaDAO();

			if (consultaEmpresa.getFieldNome().getText().isEmpty()&&consultaEmpresa.getFieldID().getText().isEmpty()) {
				consultaEmpresa.getModelTalble().setNumRows(0);
				List<Empresa> empresas = empresaController.consultaEmpresas();
				if(!empresas.isEmpty()){
					for (int i = 0; i < empresas.size(); i++) {
						consultaEmpresa.getModelTalble().addRow(new Object[]{empresas.get(i).getId(),empresas.get(i).getNome()});
					}
					consultaEmpresa.getButtonSelect().setEnabled(true);
				}else 
					JOptionPane.showMessageDialog(consultaEmpresa, "Nenhuma Empresa Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
					
				empresas = null;
			}else if (!consultaEmpresa.getFieldNome().getText().isEmpty()) {
				consultaEmpresa.getModelTalble().setNumRows(0);
				List<Empresa> empresas = empresaController.consultaNome(consultaEmpresa.getFieldNome().getText());
				if(!empresas.isEmpty()){
					for (int i = 0; i < empresas.size(); i++) {
						consultaEmpresa.getModelTalble().addRow(new Object[]{empresas.get(i).getId(),empresas.get(i).getNome()});
					}
					consultaEmpresa.getButtonSelect().setEnabled(true);
				}else 
					JOptionPane.showMessageDialog(consultaEmpresa, "Nenhuma Empresa Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
					
				empresas = null;
			}else {
				consultaEmpresa.getModelTalble().setNumRows(0);
				Empresa empresa = empresaController.consultaId(Integer.parseInt(consultaEmpresa.getFieldID().getText()));
				if(empresa != null){
					consultaEmpresa.getModelTalble().addRow(new Object[]{empresa.getId(),empresa.getNome()});
					consultaEmpresa.getButtonSelect().setEnabled(true);
				}
				else 
					JOptionPane.showMessageDialog(consultaEmpresa, "Nenhuma Empresa Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				empresa = null;
			}
		}if(e.getSource()==consultaEmpresa.getButtonSelect()) {
			telaCadastroEmpresaOK.setFields((int) (consultaEmpresa.getTableEmpresas().getValueAt(consultaEmpresa.getTableEmpresas().getSelectedRow(), 0)));
			consultaEmpresa.dispose();
		}
		
	}

}
