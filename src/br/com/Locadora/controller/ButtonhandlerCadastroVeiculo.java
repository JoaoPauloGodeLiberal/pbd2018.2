package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.com.Locadora.model.Veiculo;
import br.com.Locadora.view.TelaCadastroVeiculoOK;
import br.com.Locadora.view.TelaConsultaVeiculo;

public class ButtonhandlerCadastroVeiculo implements ActionListener {

	private TelaCadastroVeiculoOK telaCadastroVeiculo;
	//	private VeiculoController veiculoController;

	public ButtonhandlerCadastroVeiculo(TelaCadastroVeiculoOK telaCadastroVeiculo) {
		this.telaCadastroVeiculo=telaCadastroVeiculo;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()==telaCadastroVeiculo.getComboBoxTipo()){
			switch (telaCadastroVeiculo.getComboBoxTipo().getSelectedIndex()) {
			case 0:{telaCadastroVeiculo.enableFieldsPequeno(); telaCadastroVeiculo.disableFieldsPickup(); telaCadastroVeiculo.disableFieldsPickupCarga(); telaCadastroVeiculo.cleanFieldsPickup();} 
			break;
			case 1:{telaCadastroVeiculo.enableFieldsPickup(); telaCadastroVeiculo.disableFieldsPickupCarga(); telaCadastroVeiculo.cleanFieldsCarga();} 
			break;
			case 2:{telaCadastroVeiculo.enableFieldsPickupCarga(); telaCadastroVeiculo.disableFieldsPequeno(); telaCadastroVeiculo.disableFieldsPickup(); telaCadastroVeiculo.cleanFieldsPickup();} break;
			default: 
				break;
			}
		}
		if(e.getSource()==telaCadastroVeiculo.getButtonNovo()) {
			telaCadastroVeiculo.getButtonSalvar().setEnabled(true);
			telaCadastroVeiculo.getButtonExcluir().setEnabled(false);
			telaCadastroVeiculo.getButtonLocalizar().setEnabled(false);
			telaCadastroVeiculo.getButtonNovo().setEnabled(false);
			telaCadastroVeiculo.enableFields();
			telaCadastroVeiculo.enableFieldsPequeno();
			telaCadastroVeiculo.setSaveupdate(true);
		}
		if(e.getSource()==telaCadastroVeiculo.getButtonSalvar()) {
			if(telaCadastroVeiculo.validaFields()==true) {
				if(telaCadastroVeiculo.isSaveupdate()==true) {
					Veiculo veiculo = new Veiculo();
					veiculo.setNumeroChassi(telaCadastroVeiculo.getFieldChassi().getText());
					veiculo.setNumeroMotor(telaCadastroVeiculo.getFieldNumMotor().getText());
					veiculo.setAnoFabricacao(telaCadastroVeiculo.getYearChooserFabricacao().getYear());
					veiculo.setAnoModelo(telaCadastroVeiculo.getYearChooserModelo().getYear());
					veiculo.setCapacidadePassageiroos((int) telaCadastroVeiculo.getComboBoxPassageiros().getSelectedItem());
					veiculo.setCombustivel((String) telaCadastroVeiculo.getComboBoxCombustivel().getSelectedItem());
					veiculo.setCor(telaCadastroVeiculo.getFieldCor().getText());
					veiculo.setNumeroPortas(Integer.parseInt(String.valueOf(telaCadastroVeiculo.getComboBoxNumPortas().getSelectedItem()).substring(0, 1)));
					veiculo.setPlaca(telaCadastroVeiculo.getFormattedTextFieldPlaca().getText().replaceAll("[.-]", ""));
					veiculo.setTorqueDoMotor(Double.parseDouble(telaCadastroVeiculo.getFormattedTextFieldTorque().getText()));
					veiculo.setQuilometragem(Integer.parseInt(telaCadastroVeiculo.getFieldKms().getText()));
					veiculo.setCategoria(telaCadastroVeiculo.getCategorias().get(telaCadastroVeiculo.getComboBoxCategoria().getSelectedIndex()));
					switch (telaCadastroVeiculo.getComboBoxTipo().getSelectedIndex()) {
					case 0:{veiculo.setTipo('A'); 
					System.out.println("yipo a");
					veiculo.setArCondicionado(telaCadastroVeiculo.getChckbxArcondicionado().isSelected());
					veiculo.setRadio(telaCadastroVeiculo.getChckbxRadio().isSelected());
					veiculo.setDvd(telaCadastroVeiculo.getChckbxDVD().isSelected());
					veiculo.setDirecaoHidraulica(telaCadastroVeiculo.getChckbxDirecaoHidraulica().isSelected());
					veiculo.setMp3(telaCadastroVeiculo.getChckbxMP3().isSelected());
					veiculo.setCameraDeRe(telaCadastroVeiculo.getChckbxCameraRe().isSelected());
					veiculo.setTipoDeCambio((String) telaCadastroVeiculo.getComboBoTipoCambio().getSelectedItem());
					} break;
					case 1:{veiculo.setTipo('C');
					System.out.println("yipo c");
					veiculo.setArCondicionado(telaCadastroVeiculo.getChckbxArcondicionado().isSelected()); 
					veiculo.setRadio(telaCadastroVeiculo.getChckbxRadio().isSelected());
					veiculo.setDvd(telaCadastroVeiculo.getChckbxDVD().isSelected());
					veiculo.setDirecaoHidraulica(telaCadastroVeiculo.getChckbxDirecaoHidraulica().isSelected());
					veiculo.setMp3(telaCadastroVeiculo.getChckbxMP3().isSelected());
					veiculo.setCameraDeRe(telaCadastroVeiculo.getChckbxCameraRe().isSelected());
					veiculo.setTipoDeCambio((String) telaCadastroVeiculo.getComboBoTipoCambio().getSelectedItem()); 
					veiculo.setAirBag((String) telaCadastroVeiculo.getComboBoxAirBag().getSelectedItem());
					veiculo.setDirecaoAssistida(telaCadastroVeiculo.getChckbxComputadorDeBordo().isSelected()/*arrumar*/);
					veiculo.setCintosTraseirosRetrateis(telaCadastroVeiculo.getChckbxCintosRetrateis().isSelected());
					veiculo.setRodasDeLigaLeve(telaCadastroVeiculo.getChckbxRodasLigaLeve().isSelected()); 
					veiculo.setControleDePoluicao(telaCadastroVeiculo.getChckbxControlePoluicao().isSelected()); 
					}break;
					case 2:{veiculo.setTipo('V'); 
					System.out.println("yipo v");
					veiculo.setCapacidadeDeCarga(Double.parseDouble(telaCadastroVeiculo.getFieldCapacidadeCarga().getText().replace(',', '.')));
					veiculo.setDistanciaEntreEixos(Double.parseDouble(telaCadastroVeiculo.getFieldDistanciEixos().getText().replace(',', '.')));
					veiculo.setAcionamentoEmbreagem(telaCadastroVeiculo.getChckbxAcionamentoHemb().isSelected());
					veiculo.setDesempenhoDoVeiculo(Double.parseDouble(telaCadastroVeiculo.getFieldDesempenho().getText().replace(',', '.')));
					veiculo.setVolumeDeAbastecimento(Double.parseDouble(telaCadastroVeiculo.getFieldVolumeAbastecimento().getText().replace(',', '.')));
					}break;
					default:
						break;
					}
					telaCadastroVeiculo.setVeiculoUpdate(veiculo);
				}
				if(telaCadastroVeiculo.getVeiculoDAO().insert(telaCadastroVeiculo.getVeiculoUpdate())){
					JOptionPane.showMessageDialog(null, "Ve�culo Cadastrado com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
					telaCadastroVeiculo.cleanFields();
				}
				else
					JOptionPane.showMessageDialog(null, "Erro ao Cadastrar Ve�culo", "Erro Inser��o", JOptionPane.ERROR_MESSAGE);

			}else{
				telaCadastroVeiculo.getVeiculoUpdate().setID(Integer.parseInt(telaCadastroVeiculo.getFieldCodigo().getText()));
				telaCadastroVeiculo.getVeiculoUpdate().setNumeroChassi(telaCadastroVeiculo.getFieldChassi().getText());
				telaCadastroVeiculo.getVeiculoUpdate().setNumeroMotor(telaCadastroVeiculo.getFieldNumMotor().getText());
				telaCadastroVeiculo.getVeiculoUpdate().setAnoFabricacao(telaCadastroVeiculo.getYearChooserFabricacao().getYear());
				telaCadastroVeiculo.getVeiculoUpdate().setAnoModelo(telaCadastroVeiculo.getYearChooserModelo().getYear());
				telaCadastroVeiculo.getVeiculoUpdate().setCapacidadePassageiroos((int) telaCadastroVeiculo.getComboBoxPassageiros().getSelectedItem());
				telaCadastroVeiculo.getVeiculoUpdate().setCombustivel("COMBUSTIVEL");
				telaCadastroVeiculo.getVeiculoUpdate().setCombustivel((String) telaCadastroVeiculo.getComboBoxCombustivel().getSelectedItem());
				telaCadastroVeiculo.getVeiculoUpdate().setCor(telaCadastroVeiculo.getFieldCor().getText());
				telaCadastroVeiculo.getVeiculoUpdate().setNumeroPortas(Integer.parseInt(String.valueOf(telaCadastroVeiculo.getComboBoxNumPortas().getSelectedItem()).substring(0, 1)));
				telaCadastroVeiculo.getVeiculoUpdate().setPlaca(telaCadastroVeiculo.getFormattedTextFieldPlaca().getText().replaceAll("[.-]", ""));
				telaCadastroVeiculo.getVeiculoUpdate().setTorqueDoMotor(Double.parseDouble(telaCadastroVeiculo.getFormattedTextFieldTorque().getText()));
				telaCadastroVeiculo.getVeiculoUpdate().setQuilometragem(Integer.parseInt(telaCadastroVeiculo.getFieldKms().getText()));
				telaCadastroVeiculo.getVeiculoUpdate().setCategoria(telaCadastroVeiculo.getCategorias().get(telaCadastroVeiculo.getComboBoxCategoria().getSelectedIndex()));
				//					veiculoUpdate.getAcessorios().setID(veiculoUpdate.getAcessorios().getID());
				telaCadastroVeiculo.getVeiculoUpdate().setArCondicionado(telaCadastroVeiculo.getChckbxArcondicionado().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setRadio(telaCadastroVeiculo.getChckbxRadio().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setDvd(telaCadastroVeiculo.getChckbxDVD().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setDirecaoHidraulica(telaCadastroVeiculo.getChckbxDirecaoHidraulica().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setMp3(telaCadastroVeiculo.getChckbxMP3().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setCameraDeRe(telaCadastroVeiculo.getChckbxCameraRe().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setTipoDeCambio((String) telaCadastroVeiculo.getComboBoTipoCambio().getSelectedItem());
				telaCadastroVeiculo.getVeiculoUpdate().setAirBag((String) telaCadastroVeiculo.getComboBoxAirBag().getSelectedItem());
				telaCadastroVeiculo.getVeiculoUpdate().setDirecaoAssistida(telaCadastroVeiculo.getChckbxComputadorDeBordo().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setCintosTraseirosRetrateis(telaCadastroVeiculo.getChckbxCintosRetrateis().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setRodasDeLigaLeve(telaCadastroVeiculo.getChckbxRodasLigaLeve().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setControleDePoluicao(telaCadastroVeiculo.getChckbxControlePoluicao().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setCapacidadeDeCarga(Double.parseDouble(telaCadastroVeiculo.getFieldCapacidadeCarga().getText().replace(',', '.')));
				telaCadastroVeiculo.getVeiculoUpdate().setDistanciaEntreEixos(Double.parseDouble(telaCadastroVeiculo.getFieldDistanciEixos().getText().replace(',', '.')));
				telaCadastroVeiculo.getVeiculoUpdate().setAcionamentoEmbreagem(telaCadastroVeiculo.getChckbxAcionamentoHemb().isSelected());
				telaCadastroVeiculo.getVeiculoUpdate().setDesempenhoDoVeiculo(Double.parseDouble(telaCadastroVeiculo.getFieldDesempenho().getText().replace(',', '.')));
				telaCadastroVeiculo.getVeiculoUpdate().setVolumeDeAbastecimento(Double.parseDouble(telaCadastroVeiculo.getFieldVolumeAbastecimento().getText().replace(',', '.')));

				if(telaCadastroVeiculo.getVeiculoDAO().commit()){
					JOptionPane.showMessageDialog(null, "Ve�culo Alterado com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
					telaCadastroVeiculo.setVeiculoUpdate(null);
					telaCadastroVeiculo.cleanFields();
				}
				else
					JOptionPane.showMessageDialog(null, "Erro ao Alterar Ve�culo", "Erro Autaliza��o", JOptionPane.ERROR_MESSAGE);
			}
		}
		if(e.getSource()==telaCadastroVeiculo.getButtonExcluir()) {
			if (JOptionPane.showConfirmDialog(null, "Deseja Realmente Excluir ?", "Excluir Ve�culo", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==0) {
				if(telaCadastroVeiculo.getVeiculoDAO().delete(Integer.parseInt(telaCadastroVeiculo.getFieldCodigo().getText()))){
					JOptionPane.showMessageDialog(null, "Ve�culo Exclu�do com Sucesso", "Mensagem Cadastro", JOptionPane.INFORMATION_MESSAGE);
					telaCadastroVeiculo.cleanFields();
					telaCadastroVeiculo.cleanFieldsPickup();
					telaCadastroVeiculo.cleanFieldsPequeno();
					telaCadastroVeiculo.cleanFieldsCarga();
					telaCadastroVeiculo.disableFieldsPickupCarga();
					telaCadastroVeiculo.disableFieldsPequeno();
					telaCadastroVeiculo.getButtonNovo().setEnabled(true);
					telaCadastroVeiculo.getButtonExcluir().setEnabled(false);
					telaCadastroVeiculo.getButtonSalvar().setEnabled(false);
				}
				else
					JOptionPane.showMessageDialog(null, "Erro ao Deletar Ve�culo", "Erro Remo��o", JOptionPane.ERROR_MESSAGE);

			}
		}

		if(e.getSource()==telaCadastroVeiculo.getButtonCancelar()) {
			telaCadastroVeiculo.cleanFields();
			telaCadastroVeiculo.cleanFieldsPickup();
			telaCadastroVeiculo.cleanFieldsPequeno();
			telaCadastroVeiculo.cleanFieldsCarga();
			telaCadastroVeiculo.disableFieldsPickupCarga();
			telaCadastroVeiculo.disableFieldsPequeno();
			telaCadastroVeiculo.getButtonSalvar().setEnabled(false);
			telaCadastroVeiculo.getButtonNovo().setEnabled(true);
			telaCadastroVeiculo.getButtonExcluir().setEnabled(false);
			telaCadastroVeiculo.getButtonLocalizar().setEnabled(true);
		}

		if(e.getSource()==telaCadastroVeiculo.getButtonLocalizar()) {
			new TelaConsultaVeiculo(telaCadastroVeiculo).setVisible(true);
		}
	}

}


