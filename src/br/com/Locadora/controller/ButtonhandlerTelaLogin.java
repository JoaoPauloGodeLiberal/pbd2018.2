package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.Locadora.dao.UsuarioDAO;
import br.com.Locadora.model.Usuario;
import br.com.Locadora.view.TelaInicial;
import br.com.Locadora.view.TelaLogin;
import br.com.Locadora.view.TelaRecuperaSenha;

public class ButtonhandlerTelaLogin extends KeyAdapter implements ActionListener{

	private TelaLogin telalogin;
	private EntityManager managedEntity;
	private TelaRecuperaSenha telaRecuperaSenha;
	private UsuarioDAO usuarioDAO;

	public ButtonhandlerTelaLogin(TelaLogin telalogin){
		this.telalogin = telalogin;
	}

	@SuppressWarnings("deprecation")
	public boolean validaFields(){
		if (!telalogin.getFieldUser().getText().isEmpty() 
				&& !telalogin.getPasswordField().getText().isEmpty()) {
			return true;
		}else {
			return false;
		}
	}

	public Boolean logar(Usuario usuario){
		usuarioDAO = new UsuarioDAO();
		List<Usuario>users = usuarioDAO.listarTodos();

		for(Usuario u : users) {
			if(u.getLogin().equals(usuario.getLogin()) &&  Cript.validarSenha(u.getSenha(), usuario.getSenha())){
				return true;
			} 
		}

		return false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		usuarioDAO = new UsuarioDAO();
		if(e.getSource()==telalogin.getButtonEntrar()){

			if (validaFields()) {
				if (logar(new Usuario(telalogin.getFieldUser().getText(), new String(telalogin.getPasswordField().getPassword())))) {
					telalogin.dispose();
					new TelaInicial(new UsuarioDAO().consultaLogin(telalogin.getFieldUser().getText()).get(0)).setVisible(true);
					telalogin = null;
				}else{
					telalogin.showMensseger(TelaLogin.LOGIN_INVALID);
				}
			}else {
				telalogin.showMensseger(TelaLogin.FIELDS_EMPTY);
			}
		}

		else if(e.getSource()==telalogin.getEsqueciSenhaButton()){
			telalogin.setVisible(false);
			telaRecuperaSenha = new TelaRecuperaSenha();
			telaRecuperaSenha.getBtnAtualizar().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					UsuarioDAO uc = new UsuarioDAO();
					ArrayList<Usuario> us = new ArrayList<>( uc.consultaLogin(telaRecuperaSenha.getLoginF().getText()));
					for (Usuario usuario : us) {
						if(usuario.getEmail().equals(telaRecuperaSenha.getEmailf().getText())){
							String senha = new String(telaRecuperaSenha.getPasswordField().getPassword());
							usuario.setSenha(Cript.hashSenha(senha));
							uc.merge(usuario);
						}
					}telaRecuperaSenha.dispose();
					telalogin.setVisible(true);
				}
			});
		}
	}

	public KeyAdapter validarsenha(){
		KeyAdapter ke = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER){
					if (validaFields()) {
						if (logar(new Usuario(telalogin.getFieldUser().getText(), new String(telalogin.getPasswordField().getPassword())))) {
							telalogin.dispose();
							new TelaInicial(new UsuarioDAO().consultaLogin(telalogin.getFieldUser().getText()).get(0)).setVisible(true);
							telalogin = null;
						}else {
							telalogin.showMensseger(TelaLogin.LOGIN_INVALID);
						}
					}else {
						telalogin.showMensseger(TelaLogin.FIELDS_EMPTY);
					}
				}
			}
		};
		return ke;
	}

	public KeyAdapter enter(){
		KeyAdapter ke = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER){
					if (validaFields()) {
						if (logar(new Usuario(telalogin.getFieldUser().getText(), new String(telalogin.getPasswordField().getPassword())))) {
							telalogin.dispose();
							new TelaInicial(new UsuarioDAO().consultaLogin(telalogin.getFieldUser().getText()).get(0)).setVisible(true);
							telalogin = null;
						}
					}else {
						telalogin.showMensseger(TelaLogin.FIELDS_EMPTY);
					}
				}
			}
		};
		return ke;
	}



	public void closeManaged(){
		managedEntity.close();
	}

}
