package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.Locadora.dao.ClienteDAO;
import br.com.Locadora.model.Cliente;
import br.com.Locadora.model.PessoaFisica;
import br.com.Locadora.model.PessoaJuridica;
import br.com.Locadora.view.TelaCadastroClienteOK;
import br.com.Locadora.view.TelaConsultaCliente;
import br.com.Locadora.view.TelaLocacao;
import br.com.Locadora.view.TelaReserva;

public class ButtonHandlerConsultaCliente implements ActionListener {
	
	Object objectTela;
	
	private TelaConsultaCliente consultaCliente;

	private TelaCadastroClienteOK telaCliente;
	private TelaReserva telaReserva;
	private TelaLocacao telaLocacao;
	private Boolean isCliente;
	
	public ButtonHandlerConsultaCliente(TelaConsultaCliente consultaCliente,Object o, boolean isCliente) {
		this.consultaCliente = consultaCliente;
		objectTela = o;
		this.isCliente = isCliente;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ClienteDAO clienteDAO = new ClienteDAO();
		if(e.getSource()==consultaCliente.getButtonSelect()) {
			if(objectTela.getClass().equals(TelaCadastroClienteOK.class)){
				telaCliente = (TelaCadastroClienteOK) objectTela;
				if (consultaCliente.getTableClientes().getValueAt(consultaCliente.getTableClientes().getSelectedRow(), 2).equals("F�SICA")) {
					telaCliente.setFieldsPF((int) (consultaCliente.getTableClientes().getValueAt(consultaCliente.getTableClientes().getSelectedRow(), 0)));
				}else {
					telaCliente.setFieldsPJ((int) (consultaCliente.getTableClientes().getValueAt(consultaCliente.getTableClientes().getSelectedRow(), 0)));
				}
				consultaCliente.dispose();
			}else if(objectTela.getClass().equals(TelaReserva.class)){
				telaReserva = (TelaReserva) objectTela;
				telaReserva.setCliente(clienteDAO.consultaId((int) (consultaCliente.getTableClientes().getValueAt(consultaCliente.getTableClientes().getSelectedRow(), 0))));
				consultaCliente.dispose();
			}else {
				if(isCliente){
					telaLocacao = (TelaLocacao) objectTela;
					telaLocacao.setCliente(clienteDAO.consultaId((int) (consultaCliente.getTableClientes().getValueAt(consultaCliente.getTableClientes().getSelectedRow(), 0))));
					consultaCliente.dispose();
				}
				else	
					if(consultaCliente.getTableClientes().getValueAt(consultaCliente.getTableClientes().getSelectedRow(), 2).equals("F�SICA")){
						telaLocacao = (TelaLocacao) objectTela;
						telaLocacao.setMotorista(clienteDAO.consultaPFId((int) (consultaCliente.getTableClientes().getValueAt(consultaCliente.getTableClientes().getSelectedRow(), 0))));
						consultaCliente.dispose();
					}
					else{
						JOptionPane.showMessageDialog(consultaCliente, "O Motorista deve ser uma Pessoa F�sica", "Aten��o", JOptionPane.WARNING_MESSAGE);
					}
			}
			
		}if(e.getSource()==consultaCliente.getButtonResearch()) {
			
			if (consultaCliente.getFieldNome().getText().isEmpty()&&consultaCliente.getFieldID().getText().isEmpty()) {
				consultaCliente.getModelTalble().setNumRows(0);
				List<PessoaFisica> clientes = clienteDAO.consultaClientesPF();
				List<PessoaJuridica> clientesPJ = clienteDAO.consultaClientesPJ();
				if(!clientes.isEmpty()){
					for (int i = 0; i < clientes.size(); i++) {
						consultaCliente.getModelTalble().addRow(new Object[]{clientes.get(i).getId(),clientes.get(i).getNome(), "F�SICA", clientes.get(i).getCpf(), "ISENTO", clientes.get(i).getSexo()=='M'? "MASCULINO" : "FEMININO"});
					}
					
					consultaCliente.getButtonSelect().setEnabled(true);
				}
				if(!clientesPJ.isEmpty()){
					for (int i = 0; i < clientesPJ.size(); i++) {
						consultaCliente.getModelTalble().addRow(new Object[]{clientesPJ.get(i).getId(),clientesPJ.get(i).getNome(), "JUR�DICA", clientesPJ.get(i).getCnpj(), clientesPJ.get(i).getInscricaoEstadual()});
					}
					
					consultaCliente.getButtonSelect().setEnabled(true);
				}
				if(clientes.isEmpty() && clientesPJ.isEmpty())
					JOptionPane.showMessageDialog(consultaCliente, "Nenhum Cliente Encontrado","Aviso Busca",JOptionPane.WARNING_MESSAGE);

				clientes = null;
				clientesPJ = null;
				
			}else if (!consultaCliente.getFieldNome().getText().isEmpty()) {
				consultaCliente.getModelTalble().setNumRows(0);
				List<Cliente> clientes = clienteDAO.consultaNome(consultaCliente.getFieldNome().getText());
				if(!clientes.isEmpty()){
					for (int i = 0; i < clientes.size(); i++) {
						consultaCliente.getModelTalble().addRow(new Object[]{clientes.get(i).getId(),clientes.get(i).getNome()});
					}
					consultaCliente.getButtonSelect().setEnabled(true);
				}
				else
					JOptionPane.showMessageDialog(consultaCliente, "Nenhum Cliente Encontrado","Aviso Busca",JOptionPane.WARNING_MESSAGE);
			}else {
				consultaCliente.getModelTalble().setNumRows(0);
				Cliente cliente = clienteDAO.consultaId(Integer.parseInt(consultaCliente.getFieldID().getText()));
				if(cliente != null){
					consultaCliente.getModelTalble().addRow(new Object[]{cliente.getId(),cliente.getNome()});
					consultaCliente.getButtonSelect().setEnabled(true);
				}
				else
					JOptionPane.showMessageDialog(consultaCliente, "Nenhum Cliente Encontrado","Aviso Busca",JOptionPane.WARNING_MESSAGE);
			}
		}
	
	}	

}
