package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.com.Locadora.model.Empresa;
import br.com.Locadora.model.Usuario;
import br.com.Locadora.view.TelaCadastroUsuarioOK;
import br.com.Locadora.view.TelaConsultaUsuario;

public class ButtonHandlerCadastroUsuario implements ActionListener {

	private TelaCadastroUsuarioOK telaCadastroUsuario;

	public ButtonHandlerCadastroUsuario(TelaCadastroUsuarioOK telaCadastroUsuario) {
		this.telaCadastroUsuario=telaCadastroUsuario;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==telaCadastroUsuario.getButtonSalvar()) {
			salvarDados();

		}

		if(e.getSource()==telaCadastroUsuario.getButtonExcluir()) {
			if (JOptionPane.showConfirmDialog(null, "Deseja Realmente Excluir ?", "Excluir Usu�rio", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==0) {
				if(telaCadastroUsuario.getUsuarioDAO().delete(Integer.parseInt(telaCadastroUsuario.getFieldCodigo().getText()))){
					JOptionPane.showMessageDialog(null, "Usu�rio Exclu�do com Sucesso", null, JOptionPane.INFORMATION_MESSAGE);
					telaCadastroUsuario.cleanFields();
					telaCadastroUsuario.getButtonNovo().setEnabled(true);
					telaCadastroUsuario.getButtonExcluir().setEnabled(false);
					telaCadastroUsuario.getButtonSalvar().setEnabled(false);
				}else {
					JOptionPane.showMessageDialog(null, "Erro ao Deletar Usu�rio", null, JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		if(e.getSource()==telaCadastroUsuario.getButtonCancelar()) {
			telaCadastroUsuario.cleanFields();
			telaCadastroUsuario.getButtonSalvar().setEnabled(false);
			telaCadastroUsuario.getButtonNovo().setEnabled(true);
			telaCadastroUsuario.getButtonExcluir().setEnabled(false);
			telaCadastroUsuario.getButtonLocalizar().setEnabled(true);
		}

		if(e.getSource()==telaCadastroUsuario.getButtonLocalizar()) {
			new TelaConsultaUsuario(telaCadastroUsuario.getTela()).setVisible(true);
		}

		if(e.getSource()==telaCadastroUsuario.getButtonNovo()) {
			telaCadastroUsuario.cleanFields();
			telaCadastroUsuario.getButtonSalvar().setEnabled(true);
			telaCadastroUsuario.getButtonExcluir().setEnabled(false);
			telaCadastroUsuario.getButtonLocalizar().setEnabled(false);
			telaCadastroUsuario.getButtonNovo().setEnabled(false);
			telaCadastroUsuario.enableFields();
			telaCadastroUsuario.setSaveupdate(true);
		}

		if(e.getSource()==telaCadastroUsuario.getResetSenhaButton()) {
			int entrada;
			entrada = JOptionPane.showConfirmDialog(telaCadastroUsuario, "Realmente deseja resetar a senha?");
			if(entrada==0) {
				telaCadastroUsuario.getPassFieldSenha().setText("123");
				salvarDados();
				JOptionPane.showMessageDialog(null, "Senha redefinida como padr�o");
				System.out.println(telaCadastroUsuario.getPassFieldSenha());
			}else if(entrada==2) {

			}else if(entrada==3) {

			}

		}

	}

	public void salvarDados() {
		if (telaCadastroUsuario.validarFields()) {
			Usuario u = new Usuario();
			u.setNome(telaCadastroUsuario.getFieldNome().getText());
			u.setLogin(telaCadastroUsuario.getFieldLogin().getText());
			u.setEmail(telaCadastroUsuario.getFieldEmail().getText());
			u.setAdmin(telaCadastroUsuario.getChckbxAdmin().isSelected());
			u.setSenha(Cript.hashSenha(new String(telaCadastroUsuario.getPassFieldSenha().getPassword())));
			u.setEmpresa(new Empresa(Integer.parseInt(String.valueOf(telaCadastroUsuario.getComboBoxEmpresa().getSelectedItem()).substring(0, 1))));

			if(telaCadastroUsuario.isSaveupdate()){ 
				if(telaCadastroUsuario.getUsuarioDAO().insert(u))
					JOptionPane.showMessageDialog(null, "Usu�rio Cadastrado com Sucesso", null, JOptionPane.INFORMATION_MESSAGE);
				else
					JOptionPane.showMessageDialog(null, "Erro ao Inserir Usu�rio", null, JOptionPane.ERROR_MESSAGE);
			}else {
				u.setId(Integer.parseInt(telaCadastroUsuario.getFieldCodigo().getText()));
				if(telaCadastroUsuario.getUsuarioDAO().update(u))
					JOptionPane.showMessageDialog(null, "Usu�rio Alterado com Sucesso", null, JOptionPane.INFORMATION_MESSAGE);
				else
					JOptionPane.showMessageDialog(null, "Erro ao Alterar Usu�rio", null, JOptionPane.ERROR_MESSAGE);
			}

			telaCadastroUsuario.cleanFields();
			telaCadastroUsuario.getButtonNovo().setEnabled(true);
			telaCadastroUsuario.getButtonSalvar().setEnabled(false);
			telaCadastroUsuario.getButtonLocalizar().setEnabled(true);
			telaCadastroUsuario.getButtonExcluir().setEnabled(false);
		}
	}

}
