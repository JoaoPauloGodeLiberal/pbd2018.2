package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.JOptionPane;

import br.com.Locadora.dao.HibernateSingleton;
import br.com.Locadora.view.GerarRelatorio;
import br.com.Locadora.view.TelaCadastroCategoriaOK;
import br.com.Locadora.view.TelaCadastroClienteOK;
import br.com.Locadora.view.TelaCadastroEmpresaOK;
import br.com.Locadora.view.TelaCadastroUsuarioOK;
import br.com.Locadora.view.TelaCadastroVeiculoOK;
import br.com.Locadora.view.TelaInicial;
import br.com.Locadora.view.TelaLocacao;
import br.com.Locadora.view.TelaManutencaoPreco;
import br.com.Locadora.view.TelaReserva;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;

public class ButtonHandlerTelaInicial implements ActionListener {
	
	private TelaInicial telaInicial;
	private TelaCadastroClienteOK telaCadastroCliente;
	private TelaCadastroVeiculoOK telaCadastroVeiculo;
	private TelaCadastroEmpresaOK telaCadastroEmpresa;
	private TelaCadastroCategoriaOK telaCadastroCategoria;
	private TelaCadastroUsuarioOK telaCadastroUsuario;
	private TelaReserva telaReserva;
	private TelaLocacao telaLocacao;
	private TelaManutencaoPreco telaManutencaoPreco;
	private GerarRelatorio gerarRelatorio;

	

	public ButtonHandlerTelaInicial(TelaInicial telaInicial, TelaCadastroClienteOK telaCadastroCliente,
			TelaCadastroVeiculoOK telaCadastroVeiculo, TelaCadastroEmpresaOK telaCadastroEmpresa,
			TelaCadastroCategoriaOK telaCadastroCategoria, TelaCadastroUsuarioOK telaCadastroUsuario,
			TelaReserva telaReserva, TelaLocacao telaLocacao, TelaManutencaoPreco telaManutencaoPreco,
			GerarRelatorio gerarRelatorio) {
		this.telaInicial = telaInicial;
		this.telaCadastroCliente = telaCadastroCliente;
		this.telaCadastroVeiculo = telaCadastroVeiculo;
		this.telaCadastroEmpresa = telaCadastroEmpresa;
		this.telaCadastroCategoria = telaCadastroCategoria;
		this.telaCadastroUsuario = telaCadastroUsuario;
		this.telaReserva = telaReserva;
		this.telaLocacao = telaLocacao;
		this.telaManutencaoPreco = telaManutencaoPreco;
		this.gerarRelatorio = gerarRelatorio;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==telaInicial.getMnitemSair()) {
			if (JOptionPane.showConfirmDialog(null, "Deseja Realmente Sair ?", "Sair do Sistema", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==0){
				HibernateSingleton.closeFactory();
				System.exit(0);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemClientes()) {
			if (telaInicial.isCreate(telaCadastroCliente)||telaCadastroCliente.isClosed()) {
				telaInicial.setCadastroCliente(null);
				telaCadastroCliente = new TelaCadastroClienteOK();
				telaCadastroCliente.setVisible(true);
				telaInicial.getDesktopPane().add(telaCadastroCliente);
				try {
					telaCadastroCliente.setPosicao();
					telaCadastroCliente.setSelected(true);
				} catch (PropertyVetoException ex) {
					ex.printStackTrace();
				}	
			}
			telaCadastroCliente.moveToFront();
		}
		
		if(e.getSource()==telaInicial.getMnitemEmpresas()) {
			if (telaInicial.getUsuarioLogado().isAdmin()) {
				if(telaInicial.isCreate(telaCadastroEmpresa)||telaCadastroEmpresa.isClosed()){
					telaCadastroEmpresa = null;
					telaCadastroEmpresa = new TelaCadastroEmpresaOK();
					telaInicial.getDesktopPane().add(telaCadastroEmpresa);
					try {
						telaCadastroEmpresa.setPosicao();
						telaCadastroEmpresa.setSelected(true);
					} catch (PropertyVetoException ex) {
						ex.printStackTrace();
					}
				}
				telaCadastroEmpresa.moveToFront();

			}else {
				JOptionPane.showMessageDialog(null, "Usu�rio sem Permiss�o", "Aviso de Permiss�o", JOptionPane.WARNING_MESSAGE);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemVeiculos()) {
			if (telaInicial.isCreate(telaCadastroVeiculo)||telaCadastroVeiculo.isClosed()) {
				telaCadastroVeiculo = null;
				telaCadastroVeiculo = new TelaCadastroVeiculoOK();
				telaCadastroVeiculo.setVisible(true);
				telaInicial.getDesktopPane().add(telaCadastroVeiculo);
				try {
					telaCadastroVeiculo.setPosicao();
					telaCadastroVeiculo.setSelected(true);
				} catch (PropertyVetoException ex) {
					ex.printStackTrace();
				}
			}
			telaCadastroVeiculo.moveToFront();
		}
		
		if(e.getSource()==telaInicial.getMnitemCategorias()) {
			if(telaInicial.isCreate(telaCadastroCategoria)||telaCadastroCategoria.isClosed()){
				telaCadastroCategoria = null;
				telaCadastroCategoria = new TelaCadastroCategoriaOK();
				telaCadastroCategoria.setVisible(true);
				telaInicial.getDesktopPane().add(telaCadastroCategoria);
				try {
					telaCadastroCategoria.setPosicao();
					telaCadastroCategoria.setSelected(true);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			telaCadastroCategoria.moveToFront();
		}
		
		if(e.getSource()==telaInicial.getMnitemUsuarios()) {
			if (telaInicial.getUsuarioLogado().isAdmin()) {
				if (telaInicial.isCreate(telaCadastroUsuario)||telaCadastroUsuario.isClosed()) {
					telaCadastroUsuario = null;
					telaCadastroUsuario = new TelaCadastroUsuarioOK();
					telaCadastroUsuario.setVisible(true);
					telaInicial.getDesktopPane().add(telaCadastroUsuario);
					try {
						telaCadastroUsuario.setPosicao();
						telaCadastroUsuario.setSelected(true);
					} catch (PropertyVetoException ex) {
						ex.printStackTrace();
					}
				}
				telaCadastroUsuario.moveToFront();

			}else {
				JOptionPane.showMessageDialog(null, "Usu�rio sem Permiss�o","Aviso de Permiss�o", JOptionPane.WARNING_MESSAGE);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemManutencaoDeReservas()) {
			if(telaInicial.isCreate(telaReserva)||telaReserva.isClosed()){
				telaReserva = null;
				telaReserva =  new TelaReserva();
				telaReserva.setVisible(true);
				telaInicial.getDesktopPane().add(telaReserva);
				try {
					telaReserva.setPosicao();
					telaReserva.setSelected(true);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			telaReserva.moveToFront();
		}
		
		if(e.getSource()==telaInicial.getMnitemManutencaoDeLocacao()) {
			if(telaInicial.isCreate(telaLocacao)||telaLocacao.isClosed()){
				telaLocacao = null;
				telaLocacao =  new TelaLocacao();
				telaLocacao.setVisible(true);
				telaInicial.getDesktopPane().add(telaLocacao);
				try {
					telaLocacao.setPosicao();
					telaLocacao.setSelected(true);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}

			telaLocacao.moveToFront();
		}
		
		if(e.getSource()==telaInicial.getMnitemManutenoDePrecos()) {
			if(telaInicial.isCreate(telaManutencaoPreco)||telaManutencaoPreco.isClosed()){
				telaManutencaoPreco = null;
				telaManutencaoPreco =  new TelaManutencaoPreco();
				telaManutencaoPreco.setVisible(true);
				telaInicial.getDesktopPane().add(telaManutencaoPreco);
				try {
					telaManutencaoPreco.setPosicao();
					telaManutencaoPreco.setSelected(true);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}

			telaManutencaoPreco.moveToFront();
		}
		
		if(e.getSource()==telaInicial.getMnitemRClientes()) {
			try {
				if(gerarRelatorio != null)
					gerarRelatorio = null;

				gerarRelatorio = new GerarRelatorio(JasperFillManager.fillReport("relatorios/Clientes.jasper", null,ConnectionSingleton.getConnection()), false);
			} catch (JRException ex) {
				gerarRelatorio = null;
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro ao Gerar Relat�rio","Erro Relat�rio",JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemRClientesPessoaFsica()) {
			try {
				if(gerarRelatorio != null)
					gerarRelatorio = null;

				new GerarRelatorio(JasperFillManager.fillReport("relatorios/ClientesPF.jasper", null,ConnectionSingleton.getConnection()), false);
			} catch (JRException ex) {
				gerarRelatorio = null;
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro ao Gerar Relat�rio","Erro Relat�rio",JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemRClientesPessoaJurdica()) {
			try {
				if(gerarRelatorio != null)
					gerarRelatorio = null;

				new GerarRelatorio(JasperFillManager.fillReport("relatorios/ClientesPJ.jasper", null,ConnectionSingleton.getConnection()), false);
			} catch (JRException ex) {
				gerarRelatorio = null;
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro ao Gerar Relat�rio","Erro Relat�rio",JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemREmpresas()) {
			try {
				if(gerarRelatorio != null)
					gerarRelatorio = null;

				new GerarRelatorio(JasperFillManager.fillReport("relatorios/Empresas.jasper", null,ConnectionSingleton.getConnection()), false);
			} catch (JRException ex) {
				gerarRelatorio = null;
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro ao Gerar Relat�rio","Erro Relat�rio",JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemRUsuarios()) {
			try {
				if(gerarRelatorio != null)
					gerarRelatorio = null;

				new GerarRelatorio(JasperFillManager.fillReport("relatorios/Usuarios.jasper", null,ConnectionSingleton.getConnection()), false);
			} catch (JRException ex) {
				gerarRelatorio = null;
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro ao Gerar Relat�rio","Erro Relat�rio",JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemRVeiculos()) {
			try {
				if(gerarRelatorio != null)
					gerarRelatorio = null;

				new GerarRelatorio(JasperFillManager.fillReport("relatorios/Veiculos.jasper", null,ConnectionSingleton.getConnection()), false);
			} catch (JRException ex) {
				gerarRelatorio = null;
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro ao Gerar Relat�rio","Erro Relat�rio",JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if(e.getSource()==telaInicial.getMnitemSobreOSistema()) {
			JOptionPane.showMessageDialog(null, "Universidade Federal Rural de Pernambuco - UFRPE\nUnidade Acad�mica de Serra Talhada - UAST"+
					"\nProjeto Pr�tico da Cadeira: Projeto de Banco de Dados 2018.2\nProfessor: Hidelberg Oliveira Albuquerque \nDesenvolvido por: Jo�o Paulo");
		
		}
		
	}

}
