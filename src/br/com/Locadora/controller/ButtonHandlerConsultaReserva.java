package br.com.Locadora.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.Locadora.model.Reserva;
import br.com.Locadora.view.TelaConsultaReserva;
import br.com.Locadora.view.TelaLocacao;
import br.com.Locadora.view.TelaReserva;

public class ButtonHandlerConsultaReserva implements ActionListener {
	
	private TelaConsultaReserva consultaReserva;
	private TelaReserva telaReserva;
	private TelaLocacao telaLocacao;
	private Object objectTela;
	
	public ButtonHandlerConsultaReserva(TelaConsultaReserva consultaReserva, TelaReserva telaReserva, TelaLocacao telaLocacao, Object objectTela) {
		this.consultaReserva = consultaReserva;
		this.telaReserva = telaReserva;
		this.telaLocacao = telaLocacao;
		this.objectTela = objectTela;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==consultaReserva.getButtonResearch()) {
			if (consultaReserva.getFieldDescricao().getText().isEmpty()&&consultaReserva.getFieldID().getText().isEmpty()) {
				consultaReserva.getModelTalble().setNumRows(0);
				List<Reserva> reservas = consultaReserva.getReservaDAO().listALL();
				if (!reservas.isEmpty()) {
					for (int i = 0; i < reservas.size(); i++) {
						consultaReserva.getModelTalble().addRow(new Object[]{reservas.get(i).getID(), reservas.get(i).getDescricao(), reservas.get(i).getData(), reservas.get(i).getStatus()});
					}
					consultaReserva.getButtonSelect().setEnabled(true);
				}else
					JOptionPane.showMessageDialog(consultaReserva, "Nenhuma Reserva Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				reservas = null;
			}else if (!consultaReserva.getFieldDescricao().getText().isEmpty()) {
				consultaReserva.getModelTalble().setNumRows(0);
				List<Reserva> reservas = consultaReserva.getReservaDAO().consultaNome(consultaReserva.getFieldDescricao().getText());
				if (!reservas.isEmpty()) {
					for (int i = 0; i < reservas.size(); i++) {
						consultaReserva.getModelTalble().addRow(new Object[]{reservas.get(i).getID(), reservas.get(i).getDescricao(), reservas.get(i).getData(), reservas.get(i).getStatus()});
					}
					consultaReserva.getButtonSelect().setEnabled(true);
				}else
					JOptionPane.showMessageDialog(consultaReserva, "Nenhuma Reserva Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				reservas = null;
			}else {
				consultaReserva.getModelTalble().setNumRows(0);
				Reserva reserva = consultaReserva.getReservaDAO().consultaId(Integer.parseInt(consultaReserva.getFieldID().getText()));
				if(reserva !=null){
					consultaReserva.getModelTalble().addRow(new Object[]{reserva.getID(), reserva.getDescricao(), reserva.getData(), reserva.getStatus()});
					consultaReserva.getButtonSelect().setEnabled(true);
				}else
					JOptionPane.showMessageDialog(consultaReserva, "Nenhuma Reserva Encontrada", "Retorno Busca", JOptionPane.WARNING_MESSAGE);
				
				reserva = null;
			}
		}if(e.getSource()==consultaReserva.getButtonSelect()) {
			if (objectTela.getClass().equals(TelaReserva.class)) {
				telaReserva = (TelaReserva) objectTela;
				telaReserva.setFields((int) (consultaReserva.getTableReservas().getValueAt(consultaReserva.getTableReservas().getSelectedRow(), 0)));
			}else {
				telaLocacao = (TelaLocacao) objectTela;
				telaLocacao.setReserva(consultaReserva.getReservaDAO().consultaId((int) (consultaReserva.getTableReservas().getValueAt(consultaReserva.getTableReservas().getSelectedRow(), 0))));
			}

			consultaReserva.dispose();
		}
		
	}

}
