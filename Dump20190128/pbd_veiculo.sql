-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: pbd
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `veiculo`
--

DROP TABLE IF EXISTS `veiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `veiculo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO_CAMBIO` varchar(255) DEFAULT NULL,
  `ACIONAMENTO_EMBREAGEM` bit(1) DEFAULT NULL,
  `AIRBAG` varchar(255) DEFAULT NULL,
  `ANO_FABRICACAO` int(11) NOT NULL,
  `ANO_MODELO` int(11) NOT NULL,
  `ARCONDICIONADO` bit(1) DEFAULT NULL,
  `CAMERA_RE` bit(1) DEFAULT NULL,
  `CAPACIDADE_DE_CARGA` double DEFAULT NULL,
  `CAPACIDADE` int(11) NOT NULL,
  `CINTOS_TRASEIROS_RETRATEIS` bit(1) DEFAULT NULL,
  `COMBUSTIVEL` varchar(11) NOT NULL,
  `CONTROLE_POLUICAO` bit(1) DEFAULT NULL,
  `COR` varchar(255) NOT NULL,
  `DESEMPENHO_VEICULO` double DEFAULT NULL,
  `DIRECAO_ASSISTIDA` bit(1) DEFAULT NULL,
  `DIRECAO_HIDRAULICA` bit(1) DEFAULT NULL,
  `DISTANCIA_ENTRE_EIXOS` double DEFAULT NULL,
  `DVD` bit(1) DEFAULT NULL,
  `INATIVO` bit(1) DEFAULT NULL,
  `MP3` bit(1) DEFAULT NULL,
  `NUMERO_CHASSI` varchar(255) NOT NULL,
  `NUMERO_MOTOR` varchar(255) NOT NULL,
  `NUMERO_PORTAS` int(11) NOT NULL,
  `PLACA` varchar(7) NOT NULL,
  `QUILOMETRAGEM` int(11) NOT NULL,
  `RADIO` bit(1) DEFAULT NULL,
  `RODA_LIGA_LEVE` bit(1) DEFAULT NULL,
  `TIPO` char(1) NOT NULL,
  `TORQUE_MOTOR` double NOT NULL,
  `VOLUME_ABASTECIMENTO` double DEFAULT NULL,
  `IDCATEGORIA` int(11) DEFAULT NULL,
  `IDACESSORIOS` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_bwj5974j1jf1e88b7ft52adtc` (`NUMERO_CHASSI`),
  UNIQUE KEY `UK_10o1hsxbjojkbpe9rxaymaveq` (`NUMERO_MOTOR`),
  UNIQUE KEY `UK_4fwhsqgt699a9rqts0kux4xs` (`PLACA`),
  KEY `FK5c00plhlt09oc70f3dr82farv` (`IDCATEGORIA`),
  KEY `FKlrpjowvbdbiao1uywv5nxsoio` (`IDACESSORIOS`),
  CONSTRAINT `FK5c00plhlt09oc70f3dr82farv` FOREIGN KEY (`IDCATEGORIA`) REFERENCES `categoria` (`id`),
  CONSTRAINT `FKlrpjowvbdbiao1uywv5nxsoio` FOREIGN KEY (`IDACESSORIOS`) REFERENCES `acessorios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veiculo`
--

LOCK TABLES `veiculo` WRITE;
/*!40000 ALTER TABLE `veiculo` DISABLE KEYS */;
INSERT INTO `veiculo` VALUES (1,'Manual',_binary '\0',NULL,2018,2018,_binary '',_binary '',0,1,_binary '\0','Biodiesel',_binary '\0','cinza',0,_binary '\0',_binary '',0,_binary '',_binary '\0',_binary '','77','77',1,'KKK7777',777,_binary '',_binary '\0','A',7.7,0,1,NULL),(2,'Automático',_binary '\0',NULL,2018,2018,_binary '',_binary '\0',0,1,_binary '\0','Biodiesel',_binary '\0','cin',0,_binary '\0',_binary '\0',0,_binary '\0',_binary '\0',_binary '\0','11','11',1,'SSS1111',111,_binary '',_binary '\0','A',1.1,0,1,NULL),(3,'CVT',_binary '\0',NULL,2018,2018,_binary '',_binary '',0,1,_binary '\0','Biodiesel',_binary '\0','22222222eee',0,_binary '\0',_binary '',0,_binary '',_binary '\0',_binary '','222222222222222','222222222222222',1,'DDD2222',2222,_binary '',_binary '\0','A',2.2,0,1,NULL),(4,'Dualogic',_binary '\0','Duplo-Dianteira',2018,2018,_binary '',_binary '',0,6,_binary '','Biodiesel',_binary '','ffdfdf',0,_binary '',_binary '',0,_binary '',_binary '\0',_binary '','333','333',4,'FFF3333',333,_binary '',_binary '','C',3.3,0,1,NULL),(6,'Manual',_binary '\0',NULL,2016,2016,_binary '\0',_binary '\0',0,1,_binary '\0','Biodiesel',_binary '\0','preto',0,_binary '\0',_binary '\0',0,_binary '\0',_binary '\0',_binary '\0','56565','4545454',3,'GHG4545',78787,_binary '\0',_binary '\0','A',1.2,0,1,NULL);
/*!40000 ALTER TABLE `veiculo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-28 21:38:34
