-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: pbd
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `reserva`
--

DROP TABLE IF EXISTS `reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `reserva` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CANCELADA` bit(1) DEFAULT NULL,
  `DATA_MOVIMENTO` date DEFAULT NULL,
  `DATA_RETIRADA` date DEFAULT NULL,
  `DESCRICAO` varchar(50) NOT NULL,
  `EFETIVADA` bit(1) DEFAULT NULL,
  `STATUS` varchar(20) DEFAULT NULL,
  `IDCATEGORIA` int(11) DEFAULT NULL,
  `IDCLIENTE` int(11) DEFAULT NULL,
  `IDEMPRESA` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKy9252e3219p5kp1m20g6rr12` (`IDCATEGORIA`),
  KEY `FKfh35jrm9rw6o46be6ww8dyqv8` (`IDCLIENTE`),
  KEY `FKdx9t95hnoyf0i0advcxx6w788` (`IDEMPRESA`),
  CONSTRAINT `FKdx9t95hnoyf0i0advcxx6w788` FOREIGN KEY (`IDEMPRESA`) REFERENCES `empresa` (`id`),
  CONSTRAINT `FKfh35jrm9rw6o46be6ww8dyqv8` FOREIGN KEY (`IDCLIENTE`) REFERENCES `cliente` (`id`),
  CONSTRAINT `FKy9252e3219p5kp1m20g6rr12` FOREIGN KEY (`IDCATEGORIA`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserva`
--

LOCK TABLES `reserva` WRITE;
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
INSERT INTO `reserva` VALUES (2,_binary '',NULL,'2018-12-10','sa',_binary '\0',NULL,1,2,1),(3,_binary '\0',NULL,'2018-12-11','Sasasasa',_binary '\0',NULL,1,2,1),(4,_binary '',NULL,NULL,'sasasasasas',_binary '\0',NULL,1,2,1),(8,_binary '',NULL,'2019-01-29','reserva do josé',_binary '\0',NULL,1,2,2),(9,_binary '\0',NULL,'2019-01-29','ksaksk',_binary '\0',NULL,1,5,1);
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-28 21:38:34
